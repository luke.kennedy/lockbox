table! {
    objects (id) {
        id -> Uuid,
        slot_id -> Uuid,
        name -> Nullable<Text>,
    }
}

table! {
    slots (id) {
        id -> Uuid,
        name -> Nullable<Text>,
        number -> Nullable<Int4>,
    }
}

joinable!(objects -> slots (slot_id));

allow_tables_to_appear_in_same_query!(
    objects,
    slots,
);
