#[derive(Queryable)]
pub struct Slot {
    pub id: uuid::Uuid,
    pub name: Option<String>,
    pub number: Option<i32>,
}
