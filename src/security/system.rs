use core::result::Result;
use std::{collections::HashMap, convert::TryInto};

use aes_gcm::*;
use cipher::generic_array::typenum;
use rand::Rng;
use serde::{Deserialize, Serialize};
use shamirsecretsharing::*;

pub mod wrapped_argon2;

pub const KEY_SIZE: usize = 32;
pub const NONCE_SIZE: usize = 12;
pub const MAX_SHARES: usize = 255;

const DEFAULT_LANES: u32 = 4;
const DEFAULT_TIME_COST: u32 = 100;
const DEFAULT_VARIANT: argon2::Variant = argon2::Variant::Argon2id;

#[derive(Serialize, Deserialize)]
pub enum HashingAlgorithm {
    Argon2(wrapped_argon2::Config),
}

#[derive(Serialize, Deserialize)]
pub struct Share {
    pub name: String,
    pub share_id: uuid::Uuid,
    pub total_shares: u8,
    pub minimum_shares: u8,
    pub salt: [u8; 32],
    pub key_component_encrypted: Vec<u8>,
    pub nonce: [u8; 12],
    pub hashing_details: HashingAlgorithm,
}

pub fn default_argon_config<'a>() -> argon2::Config<'a> {
    argon2::Config {
        lanes: DEFAULT_LANES,
        variant: DEFAULT_VARIANT,
        hash_length: KEY_SIZE as u32,
        time_cost: DEFAULT_TIME_COST,
        thread_mode: argon2::ThreadMode::Parallel,
        ..Default::default()
    }
}

/// Hashes the input passwords to create keys, generates a new root key, and encrypts shares of that key with the password-derived keys. Returns packets of the encrypted share, the name, the m-of-n settings, and the hashing settings
pub fn create_key_components<'a>(
    names_to_passwords: &HashMap<String, String>,
    minimum_shares: u8,
    config: Option<&argon2::Config<'a>>,
) -> Result<Vec<Share>, String> {
    if DATA_SIZE < KEY_SIZE {
        return Err(format!(
            "Secret size must be at least {} bytes, found {}",
            KEY_SIZE, DATA_SIZE
        ));
    }
    // Unwrap provided config or provide default config
    let default_config = default_argon_config();
    let real_config: &argon2::Config<'a> = match config {
        Some(x) => x,
        None => &default_config,
    };
    // Check config values are usable for this process
    if real_config.hash_length < KEY_SIZE as u32 {
        return Err(format!("hash must be at least {} bytes", KEY_SIZE));
    }
    if names_to_passwords.len() > MAX_SHARES {
        return Err(String::from(format!(
            "too many requested shares, can be at most {} but found {}",
            MAX_SHARES,
            names_to_passwords.len()
        )));
    }
    // Create an ID for this set - used only for error-checking on load, not cryptographically relevant at all
    let share_set_id = uuid::Uuid::new_v4();
    // Hash each password to generate AES keys
    let mut rng = rand::thread_rng();
    let mut hashes: HashMap<String, (Vec<u8>, [u8; KEY_SIZE])> = HashMap::new();
    for (name, pwd) in names_to_passwords {
        // Use a random salt of the same length as the key to keep things simple
        let mut salt = [0u8; KEY_SIZE];
        rng.fill(&mut salt);
        let new_hash = match argon2::hash_raw(pwd.as_bytes(), &salt, real_config) {
            Ok(val) => val,
            Err(err) => return Err(format!("{}", err)),
        };
        hashes.insert(name.clone(), (new_hash, salt));
    }
    // Create the system key - used for all system encryption, to be protected by the shares. This is *the* key for the HSM.
    let mut root_key = [0u8; DATA_SIZE];
    rng.fill(&mut root_key);

    // Derive shares from this system key so m of n are required to reconstruct the real value
    let shares = match create_shares(&root_key, names_to_passwords.len() as u8, minimum_shares) {
        Ok(val) => val,
        Err(err) => return Err(format!("{}", err)),
    };
    if shares.len() != names_to_passwords.len() || shares.len() != hashes.len() {
        // Only possible if create_shares breaks
        return Err(format!(
            "created shares/hashes/input passwords are of unequal length, comparing {}, {} and {}",
            shares.len(),
            hashes.len(),
            names_to_passwords.len()
        ));
    }
    // Encrypt each share with the key derived from its password and save these packets for distribution.
    let mut packets: Vec<Share> = Vec::new();
    let mut flat_hashes: Vec<(String, Vec<u8>, [u8; KEY_SIZE])> = Vec::new();
    for (k, (v1, v2)) in hashes {
        flat_hashes.push((k, v1, v2));
    }
    for (i, share) in shares.iter().enumerate() {
        let (name, hash, salt) = match flat_hashes.get(i) {
            Some(val) => val,
            None => return Err(format!("Not found")),
        };
        // Create key from hash data
        let key: &aes_gcm::Key<typenum::U32> =
            aes_gcm::Key::from_slice(&hash.as_slice()[0..KEY_SIZE]);
        // Create cipher from key
        let gcm = aes_gcm::Aes256Gcm::new(key);
        // Generate nonce
        let mut nonce_data = [0u8; NONCE_SIZE];
        rng.fill(&mut nonce_data);
        let nonce = Nonce::from_slice(&nonce_data);
        // Copy share data to mutable buffer
        let mut encrypted: Vec<u8> = Vec::new();
        encrypted.extend(share);
        // Encrypt the data
        match gcm.encrypt_in_place(
            nonce,
            match names_to_passwords.get(name) {
                Some(val) => val,
                None => return Err(format!("Not found")),
            }
            .as_bytes(),
            &mut encrypted,
        ) {
            Ok(val) => val,
            Err(err) => return Err(format!("{}", err)),
        };
        let new_share: Share = Share {
            name: name.clone(),
            share_id: share_set_id,
            salt: *salt,
            total_shares: names_to_passwords.len() as u8,
            minimum_shares: minimum_shares,
            key_component_encrypted: encrypted,
            nonce: nonce_data,
            hashing_details: HashingAlgorithm::Argon2(wrapped_argon2::Config {
                ad: real_config.ad.to_vec(),
                hash_length: real_config.hash_length,
                lanes: real_config.lanes,
                mem_cost: real_config.mem_cost,
                secret: real_config.secret.to_vec(),
                thread_mode: match real_config.thread_mode {
                    argon2::ThreadMode::Sequential => wrapped_argon2::ThreadMode::Sequential,
                    argon2::ThreadMode::Parallel => wrapped_argon2::ThreadMode::Parallel,
                },
                time_cost: real_config.time_cost,
                variant: match real_config.variant {
                    argon2::Variant::Argon2d => wrapped_argon2::Variant::Argon2d,
                    argon2::Variant::Argon2i => wrapped_argon2::Variant::Argon2i,
                    argon2::Variant::Argon2id => wrapped_argon2::Variant::Argon2id,
                },
                version: match real_config.version {
                    argon2::Version::Version10 => wrapped_argon2::Version::Version10,
                    argon2::Version::Version13 => wrapped_argon2::Version::Version13,
                },
            }),
        };
        packets.push(new_share);
    }
    return Ok(packets);
}

pub struct ShareWithPassword<'a> {
    pub share: &'a Share,
    pub password: &'a String,
}

pub fn combine_key_components<'a>(
    names_to_shares: &HashMap<String, ShareWithPassword>,
) -> Result<[u8; KEY_SIZE], String> {
    if DATA_SIZE < KEY_SIZE {
        return Err(format!(
            "Secret size must be at least {} bytes, found {}",
            KEY_SIZE, DATA_SIZE
        ));
    }
    if names_to_shares.len() < 1 {
        return Err(format!(
            "Must provide at least one share, found {}",
            names_to_shares.len()
        ));
    }
    let mut total_shares: u8 = 0;
    let mut min_shares: u8 = 0;
    let mut share_id: uuid::Uuid = uuid::Uuid::nil();
    let mut shares: Vec<Vec<u8>> = Vec::new();
    for (name, share) in names_to_shares {
        let pwd = &share.password;
        let share = &share.share;
        if total_shares == 0 {
            total_shares = share.total_shares;
            min_shares = share.minimum_shares;
            share_id = share.share_id;
            if total_shares == 0 || min_shares == 0 || min_shares > total_shares {
                return Err(format!("Total an minimum shares must be non-zero and valid, found total={}, min={} in share named {}", total_shares, min_shares, name));
            }
            if names_to_shares.len() < min_shares as usize {
                return Err(format!("Provided number of shares is {}, but {} of {} are required to reconstruct system key for this share set.", names_to_shares.len(), min_shares, total_shares));
            }
        } else if total_shares != share.total_shares || min_shares != share.minimum_shares {
            return Err(format!("Total and minimum shares must be consistent, found total={}, min={} in share named {} after setting total={}, min={}", share.total_shares, share.minimum_shares, name, total_shares, min_shares));
        } else if share_id.ne(&share.share_id) {
            return Err(format!(
                "Constructing secret from share id {} but found component from share id {}",
                share_id, share.share_id
            ));
        }
        // Derive share-encryption key from password
        let key_data = match &share.hashing_details {
            HashingAlgorithm::Argon2(raw_config) => {
                if raw_config.hash_length < KEY_SIZE as u32 {
                    return Err(format!(
                        "Hash must be at least {} bytes long, found config for {} bytes",
                        KEY_SIZE, raw_config.hash_length
                    ));
                }
                let config = argon2::Config {
                    ad: raw_config.ad.as_slice(),
                    hash_length: raw_config.hash_length,
                    lanes: raw_config.lanes,
                    mem_cost: raw_config.mem_cost,
                    secret: raw_config.secret.as_slice(),
                    thread_mode: match raw_config.thread_mode {
                        wrapped_argon2::ThreadMode::Sequential => argon2::ThreadMode::Sequential,
                        wrapped_argon2::ThreadMode::Parallel => argon2::ThreadMode::Parallel,
                    },
                    time_cost: raw_config.time_cost,
                    variant: match raw_config.variant {
                        wrapped_argon2::Variant::Argon2d => argon2::Variant::Argon2d,
                        wrapped_argon2::Variant::Argon2i => argon2::Variant::Argon2i,
                        wrapped_argon2::Variant::Argon2id => argon2::Variant::Argon2id,
                    },
                    version: match raw_config.version {
                        wrapped_argon2::Version::Version10 => argon2::Version::Version10,
                        wrapped_argon2::Version::Version13 => argon2::Version::Version13,
                    },
                };
                let hash = match argon2::hash_raw(pwd.as_bytes(), &share.salt, &config) {
                    Ok(val) => val,
                    Err(err) => return Err(format!("{}", err)),
                };
                let hash_key: [u8; KEY_SIZE] = match hash.try_into() {
                    Ok(val) => val,
                    Err(_) => {
                        return Err(format!(
                            "Could not convert hash vector to array, length error"
                        ))
                    }
                };
                hash_key
            }
        };
        // Decrypt share
        // Create key from hash data
        let key: &aes_gcm::Key<typenum::U32> = aes_gcm::Key::from_slice(&key_data);
        // Create cipher from key
        let gcm = aes_gcm::Aes256Gcm::new(key);
        // Copy encrypted share data to mutable buffer, append to vec of shares if it works
        let mut encrypted: Vec<u8> = Vec::new();
        encrypted.extend(&share.key_component_encrypted);
        match gcm.decrypt_in_place(
            Nonce::from_slice(&share.nonce),
            pwd.as_bytes(),
            &mut encrypted,
        ) {
            Ok(_) => shares.push(encrypted),
            Err(err) => return Err(format!("{}", err)),
        };
    }
    // Combine shares and return resulting key
    match combine_shares(shares.as_slice()) {
        Ok(key) => match key {
            Some(vec_var) => {
                let key_length = vec_var.len();
                let system_key: [u8; KEY_SIZE] = match vec_var.as_slice()[0..KEY_SIZE].try_into() {
                    Ok(var) => var,
                    Err(_) => {
                        return Err(format!(
                        "Could not convert key vector to array, length error. Needed {}, found {}",
                        KEY_SIZE,
                        key_length
                    ))
                    }
                };
                return Ok(system_key);
            }
            None => {
                return Err("Combining shares silently failed, check components match".to_string());
            }
        },
        Err(err) => return Err(format!("{}", err)),
    }
}

pub fn cli_load_shares() -> [u8; KEY_SIZE] {
    use dialoguer::*;
    use std::fs::*;
    let mut shares: Vec<Share> = Vec::new();
    let share1_path: String = Input::new()
        .with_prompt("Path to first share")
        .interact_text()
        .expect("Failed to parse path to first share");
    let share1: Share = serde_json::from_slice(read(share1_path).unwrap().as_slice()).unwrap();
    println!(
        "Loaded first share, need {} of {} shares to reconstruct system key.",
        share1.minimum_shares, share1.total_shares
    );
    let share_id = share1.share_id;
    let min_shares = share1.minimum_shares;
    shares.push(share1);
    for i in 2..min_shares + 1 {
        let next_share_path: String = Input::new()
            .with_prompt("Path to next share")
            .interact_text()
            .expect("Failed to parse path to next share");
        let next_share: Share =
            serde_json::from_slice(read(next_share_path).unwrap().as_slice()).unwrap();
        if next_share.share_id != share_id {
            panic!("Found mismatched share - first share was from batch with ID {} and share #{} ({}) was from batch with ID {}", share_id, i, next_share.name, next_share.share_id);
        }
        shares.push(next_share);
    }
    let mut passwords: Vec<String> = Vec::new();
    let mut names_to_shares: HashMap<String, ShareWithPassword> = HashMap::new();
    for share in &shares {
        let pwd = Password::new()
            .with_prompt(format!("Password for share '{}'", share.name))
            .interact()
            .expect("Failed to parse password");
        if pwd.len() == 0 {
            panic!("Password cannot be empty");
        }
        passwords.push(pwd);
    }
    for (i, share) in shares.iter().enumerate() {
        let name = share.name.clone();
        let share_with_password = ShareWithPassword {
            share: share,
            password: passwords.get(i).unwrap(),
        };
        names_to_shares.insert(name, share_with_password);
    }
    println!("Reconstructing system key from shares...");
    return combine_key_components(&names_to_shares).unwrap();
}
