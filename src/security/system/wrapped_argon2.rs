use serde::{Deserialize, Serialize};
#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct Config {
    /// The associated data.
    pub ad: Vec<u8>,

    /// The length of the resulting hash.
    pub hash_length: u32,

    /// The number of lanes.
    pub lanes: u32,

    /// The amount of memory requested (KB).
    pub mem_cost: u32,

    /// The key.
    pub secret: Vec<u8>,

    /// The thread mode.
    pub thread_mode: ThreadMode,

    /// The number of passes.
    pub time_cost: u32,

    /// The variant.
    pub variant: Variant,

    /// The version number.
    pub version: Version,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum ThreadMode {
    /// Run in one thread.
    Sequential,

    /// Run in the same number of threads as the number of lanes.
    Parallel,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Deserialize, Serialize)]
pub enum Variant {
    /// Argon2 using data-dependent memory access to thwart tradeoff attacks.
    /// Recommended for cryptocurrencies and backend servers.
    Argon2d = 0,

    /// Argon2 using data-independent memory access to thwart side-channel
    /// attacks. Recommended for password hashing and password-based key
    /// derivation.
    Argon2i = 1,

    /// Argon2 using hybrid construction.
    Argon2id = 2,
}

/// The Argon2 version.
#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Deserialize, Serialize)]
pub enum Version {
    /// Version 0x10.
    Version10 = 0x10,

    /// Version 0x13 (Recommended).
    Version13 = 0x13,
}
