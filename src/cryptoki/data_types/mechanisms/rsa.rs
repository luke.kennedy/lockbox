use crate::cryptoki::data_types::objects::*;

// TODO

/** RSA private key objects (object class CKO_PRIVATE_KEY, key type CKK_RSA) hold RSA private keys.

Depending on the token, there may be limits on the length of the key components. See PKCS#1 for more information on RSA keys.

Tokens vary in what they actually store for RSA private keys. Some tokens store all of the above attributes, which can assist in performing rapid RSA computations. Other tokens might store only the CKA_MODULUS and CKA_PRIVATE_EXPONENT values. Effective with version 2.40, tokens MUST also store CKA_PUBLIC_EXPONENT. This permits the retrieval of sufficient data to reconstitute the associated public key.

Because of this, Cryptoki is flexible in dealing with RSA private key objects. When a token generates an RSA private key, it stores whichever of the fields in Table 27 it keeps track of. Later, if an application asks for the values of the key’s various attributes, Cryptoki supplies values only for attributes whose values it can obtain (i.e., if Cryptoki is asked for the value of an attribute it cannot obtain, the request fails). Note that a Cryptoki implementation may or may not be able and/or willing to supply various attributes of RSA private keys which are not actually stored on the token. E.g., if a particular token stores values only for the CKA_PRIVATE_EXPONENT, CKA_PUBLIC_EXPONENT, CKA_PRIME_1, and CKA_PRIME_2 attributes, then Cryptoki is certainly able to report values for all the attributes above (since they can all be computed efficiently from these four values). However, a Cryptoki implementation may or may not actually do this extra computation. The only attributes from Table 27 for which a Cryptoki implementation is required to be able to return values are CKA_MODULUS, CKA_PRIVATE_EXPONENT, and CKA_PUBLIC_EXPONENT. A token SHOULD also be able to return CKA_PUBLIC_KEY_INFO for an RSA private key. See the general guidance for Private Keys above.
 */
#[allow(non_snake_case)]
pub trait RSAPrivateKeyObject: PrivateKeyObject {
    /// Modulus n
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_MODULUS(&self) -> &BigInteger;
    /// Public exponent e
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_PUBLIC_EXPONENT(&self) -> &BigInteger;
    /// Private exponent d
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIVATE_EXPONENT(&self) -> &BigInteger;
    /// Prime p
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIME_1(&self) -> &BigInteger;
    /// Prime q
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIME_2(&self) -> &BigInteger;
    /// Private exponent d modulo p-1
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_EXPONENT_1(&self) -> &BigInteger;
    /// Private exponent d modulo q-1
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_EXPONENT_2(&self) -> &BigInteger;
    /// CRT coefficient q-1 mod p
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_COEFFICIENT(&self) -> &BigInteger;
}

/** RSA private key objects (object class CKO_PRIVATE_KEY, key type CKK_RSA) hold RSA private keys.

Depending on the token, there may be limits on the length of the key components. See PKCS#1 for more information on RSA keys.

Tokens vary in what they actually store for RSA private keys. Some tokens store all of the above attributes, which can assist in performing rapid RSA computations. Other tokens might store only the CKA_MODULUS and CKA_PRIVATE_EXPONENT values. Effective with version 2.40, tokens MUST also store CKA_PUBLIC_EXPONENT. This permits the retrieval of sufficient data to reconstitute the associated public key.

Because of this, Cryptoki is flexible in dealing with RSA private key objects. When a token generates an RSA private key, it stores whichever of the fields in Table 27 it keeps track of. Later, if an application asks for the values of the key’s various attributes, Cryptoki supplies values only for attributes whose values it can obtain (i.e., if Cryptoki is asked for the value of an attribute it cannot obtain, the request fails). Note that a Cryptoki implementation may or may not be able and/or willing to supply various attributes of RSA private keys which are not actually stored on the token. E.g., if a particular token stores values only for the CKA_PRIVATE_EXPONENT, CKA_PUBLIC_EXPONENT, CKA_PRIME_1, and CKA_PRIME_2 attributes, then Cryptoki is certainly able to report values for all the attributes above (since they can all be computed efficiently from these four values). However, a Cryptoki implementation may or may not actually do this extra computation. The only attributes from Table 27 for which a Cryptoki implementation is required to be able to return values are CKA_MODULUS, CKA_PRIVATE_EXPONENT, and CKA_PUBLIC_EXPONENT. A token SHOULD also be able to return CKA_PUBLIC_KEY_INFO for an RSA private key. See the general guidance for Private Keys above.
 */
#[allow(non_snake_case)]
pub trait MutRSAPrivateKeyObject: MutPrivateKeyObject {
    /// Modulus n
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_MODULUS(&mut self) -> &mut BigInteger;
    /// Public exponent e
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_PUBLIC_EXPONENT(&mut self) -> &mut BigInteger;
    /// Private exponent d
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIVATE_EXPONENT(&mut self) -> &mut BigInteger;
    /// Prime p
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIME_1(&mut self) -> &mut BigInteger;
    /// Prime q
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_PRIME_2(&mut self) -> &mut BigInteger;
    /// Private exponent d modulo p-1
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_EXPONENT_1(&mut self) -> &mut BigInteger;
    /// Private exponent d modulo q-1
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_EXPONENT_2(&mut self) -> &mut BigInteger;
    /// CRT coefficient q-1 mod p
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    ///
    /// Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
    fn CKA_COEFFICIENT(&mut self) -> &mut BigInteger;
}
