use crate::cryptoki::*;

pub type ByteArray = Vec<CK_BYTE>;
pub type BigInteger = Vec<CK_BYTE>;
pub type LocalString = Vec<CK_CHAR>;
pub type RFC2279String = Vec<CK_UTF8CHAR>;

/// BaseObject defines the attributes common to all objects
/**
Common Footnotes:

1 MUST be specified when object is created with C_CreateObject.
2 MUST not be specified when object is created with C_CreateObject.
3 MUST be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
4 MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
5 MUST be specified when object is unwrapped with C_UnwrapKey.
6 MUST not be specified when object is unwrapped with C_UnwrapKey.
7 Cannot be revealed if object has its CKA_SENSITIVE attribute set to CK_TRUE or its CKA_EXTRACTABLE attribute set to CK_FALSE.
8 May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
9 Default value is token-specific, and may depend on the values of other attributes.
10 Can only be set to CK_TRUE by the SO user.
11 Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
12 Attribute cannot be changed once set to CK_FALSE. It becomes a read only attribute.
*/
#[allow(non_snake_case)]
pub trait BaseObject {
    /// Object class (type). MUST be specified when object is created with C_CreateObject.
    fn CKA_CLASS(&self) -> &CK_OBJECT_CLASS;
}

/// MustBaseObject defines the attributes common to all mutable objects
#[allow(non_snake_case)]
pub trait MutBaseObject {
    /// Object class (type). MUST be specified when object is created with C_CreateObject.
    fn CKA_CLASS(&mut self) -> &mut CK_OBJECT_CLASS;
}

/// HardwareFeatureObject defines object class CKO_HW_FEATURE
#[allow(non_snake_case)]
pub trait HardwareFeatureObject: BaseObject {
    /// Hardware feature (type). MUST be specified when object is created with C_CreateObject.
    fn CKA_HW_FEATURE_TYPE(&self) -> &CK_HW_FEATURE_TYPE;
}

/// MutHardwareFeatureObject defines object class CKO_HW_FEATURE
#[allow(non_snake_case)]
pub trait MutHardwareFeatureObject: BaseObject {
    /// Hardware feature (type). MUST be specified when object is created with C_CreateObject.
    fn CKA_HW_FEATURE_TYPE(&mut self) -> &mut CK_HW_FEATURE_TYPE;
}

// TODO: specific hardware objects from section 4.3

/** Only the CKA_LABEL attribute can be modified after the object is created. (The CKA_TOKEN, CKA_PRIVATE, and CKA_MODIFIABLE attributes can be changed in the process of copying an object, however.)

The CKA_TOKEN attribute identifies whether the object is a token object or a session object.

When the CKA_PRIVATE attribute is CK_TRUE, a user may not access the object until the user has been authenticated to the token.

The value of the CKA_MODIFIABLE attribute determines whether or not an object is read-only.

The CKA_LABEL attribute is intended to assist users in browsing.

The value of the CKA_COPYABLE attribute determines whether or not an object can be copied. This attribute can be used in conjunction with CKA_MODIFIABLE to prevent changes to the permitted usages of keys and other objects.

The value of the CKA_DESTROYABLE attribute determines whether the object can be destroyed using C_DestroyObject.
 */
#[allow(non_snake_case)]
pub trait StorageObject: BaseObject {
    /// CK_TRUE if object is a token object; CK_FALSE if object is a session object. Default is CK_FALSE.
    fn CKA_TOKEN(&self) -> &CK_BBOOL;
    /// CK_TRUE if object is a private object; CK_FALSE if object is a public object. Default value is token-specific, and may depend on the values of other attributes of the object.
    fn CKA_PRIVATE(&self) -> &CK_BBOOL;
    /// CK_TRUE if object can be modified Default is CK_TRUE.
    fn CKA_MODIFIABLE(&self) -> &CK_BBOOL;
    /// Description of the object (default empty).
    fn CKA_LABEL(&self) -> &RFC2279String;
    /// CK_TRUE if object can be copied using C_CopyObject. Defaults to CK_TRUE. Can’t be set to TRUE once it is set to FALSE.
    fn CKA_COPYABLE(&self) -> &CK_BBOOL;
    /// CK_TRUE if the object can be destroyed using C_DestroyObject. Default is CK_TRUE.
    fn CKA_DESTROYABLE(&self) -> &CK_BBOOL;
    /// The unique identifier assigned to the object.
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_UNIQUE_ID(&self) -> &RFC2279String;
}

/** Only the CKA_LABEL attribute can be modified after the object is created. (The CKA_TOKEN, CKA_PRIVATE, and CKA_MODIFIABLE attributes can be changed in the process of copying an object, however.)

The CKA_TOKEN attribute identifies whether the object is a token object or a session object.

When the CKA_PRIVATE attribute is CK_TRUE, a user may not access the object until the user has been authenticated to the token.

The value of the CKA_MODIFIABLE attribute determines whether or not an object is read-only.

The CKA_LABEL attribute is intended to assist users in browsing.

The value of the CKA_COPYABLE attribute determines whether or not an object can be copied. This attribute can be used in conjunction with CKA_MODIFIABLE to prevent changes to the permitted usages of keys and other objects.

The value of the CKA_DESTROYABLE attribute determines whether the object can be destroyed using C_DestroyObject.
 */
#[allow(non_snake_case)]
pub trait MutStorageObject: MutBaseObject {
    /// CK_TRUE if object is a token object; CK_FALSE if object is a session object. Default is CK_FALSE.
    fn CKA_TOKEN(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if object is a private object; CK_FALSE if object is a public object. Default value is token-specific, and may depend on the values of other attributes of the object.
    fn CKA_PRIVATE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if object can be modified Default is CK_TRUE.
    fn CKA_MODIFIABLE(&mut self) -> &mut CK_BBOOL;
    /// Description of the object (default empty).
    fn CKA_LABEL(&mut self) -> &mut RFC2279String;
    /// CK_TRUE if object can be copied using C_CopyObject. Defaults to CK_TRUE. Can’t be set to TRUE once it is set to FALSE.
    fn CKA_COPYABLE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if the object can be destroyed using C_DestroyObject. Default is CK_TRUE.
    fn CKA_DESTROYABLE(&mut self) -> &mut CK_BBOOL;
    /// The unique identifier assigned to the object.
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_UNIQUE_ID(&mut self) -> &mut RFC2279String;
}

/**The CKA_APPLICATION attribute provides a means for applications to indicate ownership of the data objects they manage. Cryptoki does not provide a means of ensuring that only a particular application has access to a data object, however.

The CKA_OBJECT_ID attribute provides an application independent and expandable way to indicate the type of the data object value. Cryptoki does not provide a means of insuring that the data object identifier matches the data value.
 */
#[allow(non_snake_case)]
pub trait DataObject: StorageObject {
    /// Description of the application that manages the object (default empty)
    fn CKA_APPLICATION(&self) -> &RFC2279String;
    /// DER-encoding of the object identifier indicating the data object type (default empty)
    fn CKA_OBJECT_ID(&self) -> &Vec<CK_BYTE>;
    /// Value of the object (default empty)
    fn CKA_VALUE(&self) -> &Vec<CK_BYTE>;
}

/**The CKA_APPLICATION attribute provides a means for applications to indicate ownership of the data objects they manage. Cryptoki does not provide a means of ensuring that only a particular application has access to a data object, however.

The CKA_OBJECT_ID attribute provides an application independent and expandable way to indicate the type of the data object value. Cryptoki does not provide a means of insuring that the data object identifier matches the data value.
 */
#[allow(non_snake_case)]
pub trait MutDataObject: MutStorageObject {
    /// Description of the application that manages the object (default empty)
    fn CKA_APPLICATION(&mut self) -> &mut RFC2279String;
    /// DER-encoding of the object identifier indicating the data object type (default empty)
    fn CKA_OBJECT_ID(&mut self) -> &mut Vec<CK_BYTE>;
    /// Value of the object (default empty)
    fn CKA_VALUE(&mut self) -> &mut Vec<CK_BYTE>;
}

/** Cryptoki does not enforce the relationship of the CKA_PUBLIC_KEY_INFO to the public key in the certificate, but does recommend that the key be extracted from the certificate to create this value.

The CKA_CERTIFICATE_TYPE attribute may not be modified after an object is created. This version of Cryptoki supports the following certificate types:

- X.509 public key certificate
- WTLS public key certificate
- X.509 attribute certificate

The CKA_TRUSTED attribute cannot be set to CK_TRUE by an application. It MUST be set by a token initialization application or by the token’s SO. Trusted certificates cannot be modified.

The CKA_CERTIFICATE_CATEGORY attribute is used to indicate if a stored certificate is a user certificate for which the corresponding private key is available on the token (“token user”), a CA certificate (“authority”), or another end-entity certificate (“other entity”). This attribute may not be modified after an object is created.

The CKA_CERTIFICATE_CATEGORY and CKA_TRUSTED attributes will together be used to map to the categorization of the certificates.

CKA_CHECK_VALUE: The value of this attribute is derived from the certificate by taking the first three bytes of the SHA-1 hash of the certificate object’s CKA_VALUE attribute.

The CKA_START_DATE and CKA_END_DATE attributes are for reference only; Cryptoki does not attach any special meaning to them. When present, the application is responsible to set them to values that match the certificate’s encoded “not before” and “not after” fields (if any).
 */
#[allow(non_snake_case)]
pub trait CertificateObject: StorageObject {
    /// Type of certificate. MUST be specified when object is created with C_CreateObject.
    fn CKA_CERTIFICATE_TYPE(&self) -> &CK_CERTIFICATE_TYPE;
    /// The certificate can be trusted for the application that it was created. Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&self) -> &CK_BBOOL;
    /// (default CK_CERTIFICATE_CATEGORY_UNSPECIFIED)
    fn CKA_CERTIFICATE_CATEGORY(&self) -> &CK_CERTIFICATE_CATEGORY;
    /// Checksum
    fn CKA_CHECK_VALUE(&self) -> &ByteArray;
    /// Start date for the certificate (default empty)
    fn CKA_START_DATE(&self) -> &CK_DATE;
    /// End date for the certificate (default empty)
    fn CKA_END_DATE(&self) -> &CK_DATE;
    /// DER-encoding of the SubjectPublicKeyInfo for the public key contained in this certificate (default empty)
    fn CKA_PUBLIC_KEY_INFO(&self) -> &ByteArray;
}

/** Cryptoki does not enforce the relationship of the CKA_PUBLIC_KEY_INFO to the public key in the certificate, but does recommend that the key be extracted from the certificate to create this value.

The CKA_CERTIFICATE_TYPE attribute may not be modified after an object is created. This version of Cryptoki supports the following certificate types:

- X.509 public key certificate
- WTLS public key certificate
- X.509 attribute certificate

The CKA_TRUSTED attribute cannot be set to CK_TRUE by an application. It MUST be set by a token initialization application or by the token’s SO. Trusted certificates cannot be modified.

The CKA_CERTIFICATE_CATEGORY attribute is used to indicate if a stored certificate is a user certificate for which the corresponding private key is available on the token (“token user”), a CA certificate (“authority”), or another end-entity certificate (“other entity”). This attribute may not be modified after an object is created.

The CKA_CERTIFICATE_CATEGORY and CKA_TRUSTED attributes will together be used to map to the categorization of the certificates.

CKA_CHECK_VALUE: The value of this attribute is derived from the certificate by taking the first three bytes of the SHA-1 hash of the certificate object’s CKA_VALUE attribute.

The CKA_START_DATE and CKA_END_DATE attributes are for reference only; Cryptoki does not attach any special meaning to them. When present, the application is responsible to set them to values that match the certificate’s encoded “not before” and “not after” fields (if any).
 */
#[allow(non_snake_case)]
pub trait MutCertificateObject: MutStorageObject {
    /// Type of certificate. MUST be specified when object is created with C_CreateObject.
    fn CKA_CERTIFICATE_TYPE(&mut self) -> &mut CK_CERTIFICATE_TYPE;
    /// The certificate can be trusted for the application that it was created. Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&mut self) -> &mut CK_BBOOL;
    /// (default CK_CERTIFICATE_CATEGORY_UNSPECIFIED)
    fn CKA_CERTIFICATE_CATEGORY(&mut self) -> &mut CK_CERTIFICATE_CATEGORY;
    /// Checksum
    fn CKA_CHECK_VALUE(&mut self) -> &mut ByteArray;
    /// Start date for the certificate (default empty)
    fn CKA_START_DATE(&mut self) -> &mut CK_DATE;
    /// End date for the certificate (default empty)
    fn CKA_END_DATE(&mut self) -> &mut CK_DATE;
    /// DER-encoding of the SubjectPublicKeyInfo for the public key contained in this certificate (default empty)
    fn CKA_PUBLIC_KEY_INFO(&mut self) -> &mut ByteArray;
}

/** X.509 certificate objects (certificate type CKC_X_509) hold X.509 public key certificates.

Only the CKA_ID, CKA_ISSUER, and CKA_SERIAL_NUMBER attributes may be modified after the object is created.

The CKA_ID attribute is intended as a means of distinguishing multiple public-key/private-key pairs held by the same subject (whether stored in the same token or not). (Since the keys are distinguished by subject name as well as identifier, it is possible that keys for different subjects may have the same CKA_ID value without introducing any ambiguity.)

It is intended in the interests of interoperability that the subject name and key identifier for a certificate will be the same as those for the corresponding public and private keys (though it is not required that all be stored in the same token). However, Cryptoki does not enforce this association, or even the uniqueness of the key identifier for a given subject; in particular, an application may leave the key identifier empty.

The CKA_ISSUER and CKA_SERIAL_NUMBER attributes are for compatibility with PKCS #7 and Privacy Enhanced Mail (RFC1421). Note that with the version 3 extensions to X.509 certificates, the key identifier may be carried in the certificate. It is intended that the CKA_ID value be identical to the key identifier in such a certificate extension, although this will not be enforced by Cryptoki.

The CKA_URL attribute enables the support for storage of the URL where the certificate can be found instead of the certificate itself. Storage of a URL instead of the complete certificate is often used in mobile environments.

The CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY attributes are used to store the hashes of the public keys of the subject and the issuer. They are particularly important when only the URL is available to be able to correlate a certificate with a private key and when searching for the certificate of the issuer. The hash algorithm is defined by CKA_NAME_HASH_ALGORITHM.

The CKA_JAVA_MIDP_SECURITY_DOMAIN attribute associates a certificate with a Java MIDP security domain.
 */
#[allow(non_snake_case)]
pub trait X509PublicKeyCertificateObject: CertificateObject {
    /// DER-encoding of the certificate subject name. MUST be specified when the object is created.
    fn CKA_SUBJECT(&self) -> &ByteArray;
    /// Key identifier for public/private key pair (default empty)
    fn CKA_ID(&self) -> &ByteArray;
    /// DER-encoding of the certificate issuer name (default empty)
    fn CKA_ISSUER(&self) -> &ByteArray;
    /// DER-encoding of the certificate serial number (default empty)
    fn CKA_SERIAL_NUMBER(&self) -> &ByteArray;
    /// BER-encoding of the certificate.
    fn CKA_VALUE(&self) -> &ByteArray;
    /// MUST be specified when the object is created. MUST be non-empty if CKA_URL is empty.
    fn CKA_URL(&self) -> &RFC2279String;
    /// Hash of the subject public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITH. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_SUBJECT_PUBLIC_KEY(&self) -> &ByteArray;
    /// Hash of the issuer public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_ISSUER_PUBLIC_KEY(&self) -> &ByteArray;
    /// Java MIDP security domain. (default CK_SECURITY_DOMAIN_UNSPECIFIED)
    fn CKA_JAVA_MIDP_SECURITY_DOMAIN(&self) -> &CK_JAVA_MIDP_SECURITY_DOMAIN;
    /// Defines the mechanism used to calculate CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY. If the attribute is not present then the type defaults to SHA-1.
    fn CKA_NAME_HASH_ALGORITHM(&self) -> &CK_MECHANISM_TYPE;
}

/** X.509 certificate objects (certificate type CKC_X_509) hold X.509 public key certificates.

Only the CKA_ID, CKA_ISSUER, and CKA_SERIAL_NUMBER attributes may be modified after the object is created.

The CKA_ID attribute is intended as a means of distinguishing multiple public-key/private-key pairs held by the same subject (whether stored in the same token or not). (Since the keys are distinguished by subject name as well as identifier, it is possible that keys for different subjects may have the same CKA_ID value without introducing any ambiguity.)

It is intended in the interests of interoperability that the subject name and key identifier for a certificate will be the same as those for the corresponding public and private keys (though it is not required that all be stored in the same token). However, Cryptoki does not enforce this association, or even the uniqueness of the key identifier for a given subject; in particular, an application may leave the key identifier empty.

The CKA_ISSUER and CKA_SERIAL_NUMBER attributes are for compatibility with PKCS #7 and Privacy Enhanced Mail (RFC1421). Note that with the version 3 extensions to X.509 certificates, the key identifier may be carried in the certificate. It is intended that the CKA_ID value be identical to the key identifier in such a certificate extension, although this will not be enforced by Cryptoki.

The CKA_URL attribute enables the support for storage of the URL where the certificate can be found instead of the certificate itself. Storage of a URL instead of the complete certificate is often used in mobile environments.

The CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY attributes are used to store the hashes of the public keys of the subject and the issuer. They are particularly important when only the URL is available to be able to correlate a certificate with a private key and when searching for the certificate of the issuer. The hash algorithm is defined by CKA_NAME_HASH_ALGORITHM.

The CKA_JAVA_MIDP_SECURITY_DOMAIN attribute associates a certificate with a Java MIDP security domain.
 */
#[allow(non_snake_case)]
pub trait MutX509PublicKeyCertificateObject: MutCertificateObject {
    /// DER-encoding of the certificate subject name. MUST be specified when the object is created.
    fn CKA_SUBJECT(&mut self) -> &mut ByteArray;
    /// Key identifier for public/private key pair (default empty)
    fn CKA_ID(&mut self) -> &mut ByteArray;
    /// DER-encoding of the certificate issuer name (default empty)
    fn CKA_ISSUER(&mut self) -> &mut ByteArray;
    /// DER-encoding of the certificate serial number (default empty)
    fn CKA_SERIAL_NUMBER(&mut self) -> &mut ByteArray;
    /// BER-encoding of the certificate.
    fn CKA_VALUE(&mut self) -> &mut ByteArray;
    /// MUST be specified when the object is created. MUST be non-empty if CKA_URL is empty.
    fn CKA_URL(&mut self) -> &mut RFC2279String;
    /// Hash of the subject public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITH. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_SUBJECT_PUBLIC_KEY(&mut self) -> &mut ByteArray;
    /// Hash of the issuer public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_ISSUER_PUBLIC_KEY(&mut self) -> &mut ByteArray;
    /// Java MIDP security domain. (default CK_SECURITY_DOMAIN_UNSPECIFIED)
    fn CKA_JAVA_MIDP_SECURITY_DOMAIN(&mut self) -> &mut CK_JAVA_MIDP_SECURITY_DOMAIN;
    /// Defines the mechanism used to calculate CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY. If the attribute is not present then the type defaults to SHA-1.
    fn CKA_NAME_HASH_ALGORITHM(&mut self) -> &mut CK_MECHANISM_TYPE;
}

/** WTLS certificate objects (certificate type CKC_WTLS) hold WTLS public key certificates.

Only the CKA_ISSUER attribute may be modified after the object has been created.

The encoding for the CKA_SUBJECT, CKA_ISSUER, and CKA_VALUE attributes can be found in [WTLS].

The CKA_URL attribute enables the support for storage of the URL where the certificate can be found instead of the certificate itself. Storage of a URL instead of the complete certificate is often used in mobile environments.

The CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY attributes are used to store the hashes of the public keys of the subject and the issuer. They are particularly important when only the URL is available to be able to correlate a certificate with a private key and when searching for the certificate of the issuer. The hash algorithm is defined by CKA_NAME_HASH_ALGORITHM.
 */
#[allow(non_snake_case)]
pub trait WTLSPublicKeyCertificateObject: CertificateObject {
    /// WTLS-encoding (Identifier type) of the certificate subject. MUST be specified when the object is created. Can only be empty if CKA_VALUE is empty.
    fn CKA_SUBJECT(&self) -> &ByteArray;
    /// WTLS-encoding (Identifier type) of the certificate issuer (default empty).
    fn CKA_ISSUER(&self) -> &ByteArray;
    /// WTLS-encoding of the certificate. MUST be specified when the object is created. MUST be non-empty if CKA_URL is empty.
    fn CKA_VALUE(&self) -> &ByteArray;
    /// If not empty this attribute gives the URL where the complete certificate can be obtained. MUST be non-empty if CKA_VALUE is empty.
    fn CKA_URL(&self) -> &RFC2279String;
    /// SHA-1 hash of the subject public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_SUBJECT_PUBLIC_KEY(&self) -> &ByteArray;
    /// SHA-1 hash of the issuer public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_ISSUER_PUBLIC_KEY(&self) -> &ByteArray;
    /// Defines the mechanism used to calculate CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY. If the attribute is not present then the type defaults to SHA-1.
    fn CKA_NAME_HASH_ALGORITHM(&self) -> &CK_MECHANISM_TYPE;
}

/** WTLS certificate objects (certificate type CKC_WTLS) hold WTLS public key certificates.

Only the CKA_ISSUER attribute may be modified after the object has been created.

The encoding for the CKA_SUBJECT, CKA_ISSUER, and CKA_VALUE attributes can be found in [WTLS].

The CKA_URL attribute enables the support for storage of the URL where the certificate can be found instead of the certificate itself. Storage of a URL instead of the complete certificate is often used in mobile environments.

The CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY attributes are used to store the hashes of the public keys of the subject and the issuer. They are particularly important when only the URL is available to be able to correlate a certificate with a private key and when searching for the certificate of the issuer. The hash algorithm is defined by CKA_NAME_HASH_ALGORITHM.
 */
#[allow(non_snake_case)]
pub trait MutWTLSPublicKeyCertificateObject: MutCertificateObject {
    /// WTLS-encoding (Identifier type) of the certificate subject. MUST be specified when the object is created. Can only be empty if CKA_VALUE is empty.
    fn CKA_SUBJECT(&mut self) -> &mut ByteArray;
    /// WTLS-encoding (Identifier type) of the certificate issuer (default empty).
    fn CKA_ISSUER(&mut self) -> &mut ByteArray;
    /// WTLS-encoding of the certificate. MUST be specified when the object is created. MUST be non-empty if CKA_URL is empty.
    fn CKA_VALUE(&mut self) -> &mut ByteArray;
    /// If not empty this attribute gives the URL where the complete certificate can be obtained. MUST be non-empty if CKA_VALUE is empty.
    fn CKA_URL(&mut self) -> &mut RFC2279String;
    /// SHA-1 hash of the subject public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_SUBJECT_PUBLIC_KEY(&mut self) -> &mut ByteArray;
    /// SHA-1 hash of the issuer public key (default empty). Hash algorithm is defined by CKA_NAME_HASH_ALGORITHM. Can only be empty if CKA_URL is empty.
    fn CKA_HASH_OF_ISSUER_PUBLIC_KEY(&mut self) -> &mut ByteArray;
    /// Defines the mechanism used to calculate CKA_HASH_OF_SUBJECT_PUBLIC_KEY and CKA_HASH_OF_ISSUER_PUBLIC_KEY. If the attribute is not present then the type defaults to SHA-1.
    fn CKA_NAME_HASH_ALGORITHM(&mut self) -> &mut CK_MECHANISM_TYPE;
}

/** X.509 attribute certificate objects (certificate type CKC_X_509_ATTR_CERT) hold X.509 attribute certificates.

Only the CKA_AC_ISSUER, CKA_SERIAL_NUMBER and CKA_ATTR_TYPES attributes may be modified after the object is created.
 */
#[allow(non_snake_case)]
pub trait X509AttributeCertificateObject: CertificateObject {
    /// DER-encoding of the attribute certificate's subject field. This is distinct from the CKA_SUBJECT attribute contained in CKC_X_509 certificates because the ASN.1 syntax and encoding are different. MUST be specified when the object is created.
    fn CKA_OWNER(&self) -> &ByteArray;
    /// DER-encoding of the attribute certificate's issuer field. This is distinct from the CKA_ISSUER attribute contained in CKC_X_509 certificates because the ASN.1 syntax and encoding are different. (default empty)
    fn CKA_AC_ISSUER(&self) -> &ByteArray;
    /// DER-encoding of the certificate serial number. (default empty)
    fn CKA_SERIAL_NUMBER(&self) -> &ByteArray;
    /// BER-encoding of a sequence of object identifier values corresponding to the attribute types contained in the certificate. When present, this field offers an opportunity for applications to search for a particular attribute certificate without fetching and parsing the certificate itself. (default empty)
    fn CKA_ATTR_TYPES(&self) -> &ByteArray;
    /// BER-encoding of the certificate.
    fn CKA_VALUE(&self) -> &ByteArray;
}

/** X.509 attribute certificate objects (certificate type CKC_X_509_ATTR_CERT) hold X.509 attribute certificates.

Only the CKA_AC_ISSUER, CKA_SERIAL_NUMBER and CKA_ATTR_TYPES attributes may be modified after the object is created.
 */
#[allow(non_snake_case)]
pub trait MutX509AttributeCertificateObject: MutCertificateObject {
    /// DER-encoding of the attribute certificate's subject field. This is distinct from the CKA_SUBJECT attribute contained in CKC_X_509 certificates because the ASN.1 syntax and encoding are different. MUST be specified when the object is created.
    fn CKA_OWNER(&mut self) -> &mut ByteArray;
    /// DER-encoding of the attribute certificate's issuer field. This is distinct from the CKA_ISSUER attribute contained in CKC_X_509 certificates because the ASN.1 syntax and encoding are different. (default empty)
    fn CKA_AC_ISSUER(&mut self) -> &mut ByteArray;
    /// DER-encoding of the certificate serial number. (default empty)
    fn CKA_SERIAL_NUMBER(&mut self) -> &mut ByteArray;
    /// BER-encoding of a sequence of object identifier values corresponding to the attribute types contained in the certificate. When present, this field offers an opportunity for applications to search for a particular attribute certificate without fetching and parsing the certificate itself. (default empty)
    fn CKA_ATTR_TYPES(&mut self) -> &mut ByteArray;
    /// BER-encoding of the certificate.
    fn CKA_VALUE(&mut self) -> &mut ByteArray;
}

/** Key objects hold encryption or authentication keys, which can be public keys, private keys, or secret keys. The following common footnotes apply to all the tables describing attributes of key
 */
#[allow(non_snake_case)]
pub trait KeyObject: StorageObject {
    /// Type of key.
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_KEY_TYPE(&self) -> &CK_KEY_TYPE;
    /// Key identifier for key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_ID(&self) -> &CK_KEY_TYPE;
    /// Start date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_START_DATE(&self) -> &CK_KEY_TYPE;
    /// End date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_END_DATE(&self) -> &CK_KEY_TYPE;
    /// CK_TRUE if key supports key derivation (i.e., if other keys can be derived from this one (default CK_FALSE)
    /// End date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_DERIVE(&self) -> &CK_KEY_TYPE;
    /** CK_TRUE only if key was either

    - generated locally (i.e., on the token) with a C_GenerateKey or C_GenerateKeyPair call
    - created with a C_CopyObject call as a copy of a key which had its CKA_LOCAL attribute set to CK_TRUE

    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.

    MUST not be specified when object is unwrapped with C_UnwrapKey.
    */
    fn CKA_LOCAL(&self) -> &CK_KEY_TYPE;
    /// Identifier of the mechanism used to generate the key material.
    /**
    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.

    MUST not be specified when object is unwrapped with C_UnwrapKey.
    */
    fn CKA_KEY_GEN_MECHANISM(&self) -> &CK_KEY_TYPE;
    /// A list of mechanisms allowed to be used with this key. The number of mechanisms in the array is the ulValueLen component of the attribute divided by the size of CK_MECHANISM_TYPE.
    fn CKA_ALLOWED_MECHANISMS(&self) -> &CK_KEY_TYPE;
}

/** Key objects hold encryption or authentication keys, which can be public keys, private keys, or secret keys. The following common footnotes apply to all the tables describing attributes of key
 */
#[allow(non_snake_case)]
pub trait MutKeyObject: MutStorageObject {
    /// Type of key.
    ///
    /// MUST be specified when object is created with C_CreateObject.
    ///
    /// MUST be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_KEY_TYPE(&mut self) -> &mut CK_KEY_TYPE;
    /// Key identifier for key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_ID(&mut self) -> &mut CK_KEY_TYPE;
    /// Start date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_START_DATE(&mut self) -> &mut CK_KEY_TYPE;
    /// End date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_END_DATE(&mut self) -> &mut CK_KEY_TYPE;
    /// CK_TRUE if key supports key derivation (i.e., if other keys can be derived from this one (default CK_FALSE)
    /// End date for the key (default empty).
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_DERIVE(&mut self) -> &mut CK_KEY_TYPE;
    /** CK_TRUE only if key was either

    - generated locally (i.e., on the token) with a C_GenerateKey or C_GenerateKeyPair call
    - created with a C_CopyObject call as a copy of a key which had its CKA_LOCAL attribute set to CK_TRUE

    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.

    MUST not be specified when object is unwrapped with C_UnwrapKey.
    */
    fn CKA_LOCAL(&mut self) -> &mut CK_KEY_TYPE;
    /// Identifier of the mechanism used to generate the key material.
    /**
    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.

    MUST not be specified when object is unwrapped with C_UnwrapKey.
    */
    fn CKA_KEY_GEN_MECHANISM(&mut self) -> &mut CK_KEY_TYPE;
    /// A list of mechanisms allowed to be used with this key. The number of mechanisms in the array is the ulValueLen component of the attribute divided by the size of CK_MECHANISM_TYPE.
    fn CKA_ALLOWED_MECHANISMS(&mut self) -> &mut CK_KEY_TYPE;
}

/// Public key objects (object class CKO_PUBLIC_KEY) hold public keys
#[allow(non_snake_case)]
pub trait PublicKeyObject: KeyObject {
    /// DER-encoding of the key subject name (default empty)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_SUBJECT(&self) -> &ByteArray;
    /// CK_TRUE if key supports encryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_ENCRYPT(&self) -> &ByteArray;
    /// CK_TRUE if key supports verification where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY(&self) -> &ByteArray;
    /// CK_TRUE if key supports verification where the data is recovered from the signature
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY_RECOVER(&self) -> &ByteArray;
    /// CK_TRUE if key supports wrapping (i.e., can be used to wrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_WRAP(&self) -> &ByteArray;
    /// The key can be trusted for the application that it was created.
    ///
    /// The wrapping key can be used to wrap keys with CKA_WRAP_WITH_TRUSTED set to CK_TRUE.
    ///
    /// Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&self) -> &ByteArray;
    /// For wrapping keys. The attribute template to match against any keys wrapped using this wrapping key. Keys that do not match cannot be wrapped. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_WRAP_TEMPLATE(&self) -> &ByteArray;
    ///DER-encoding of the SubjectPublicKeyInfo for this public key. (MAY be empty, DEFAULT derived from the underlying public key data)
    /**
    SubjectPublicKeyInfo ::= SEQUENCE {
        algorithm          AlgorithmIdentifier,
        subjectPublicKey   BIT_STRING
    }

    The encodings for the subjectPublicKey field are specified in the description of the public key types.
     */
    fn CKA_PUBLIC_KEY_INFO(&self) -> &ByteArray;
}

/// Public key objects (object class CKO_PUBLIC_KEY) hold public keys
#[allow(non_snake_case)]
pub trait MutPublicKeyObject: MutKeyObject {
    /// DER-encoding of the key subject name (default empty)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_SUBJECT(&mut self) -> &mut ByteArray;
    /// CK_TRUE if key supports encryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_ENCRYPT(&mut self) -> &mut ByteArray;
    /// CK_TRUE if key supports verification where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY(&mut self) -> &mut ByteArray;
    /// CK_TRUE if key supports verification where the data is recovered from the signature
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY_RECOVER(&mut self) -> &mut ByteArray;
    /// CK_TRUE if key supports wrapping (i.e., can be used to wrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_WRAP(&mut self) -> &mut ByteArray;
    /// The key can be trusted for the application that it was created.
    ///
    /// The wrapping key can be used to wrap keys with CKA_WRAP_WITH_TRUSTED set to CK_TRUE.
    ///
    /// Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&mut self) -> &mut ByteArray;
    /// For wrapping keys. The attribute template to match against any keys wrapped using this wrapping key. Keys that do not match cannot be wrapped. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_WRAP_TEMPLATE(&mut self) -> &mut ByteArray;
    ///DER-encoding of the SubjectPublicKeyInfo for this public key. (MAY be empty, DEFAULT derived from the underlying public key data)
    /**
    SubjectPublicKeyInfo ::= SEQUENCE {
        algorithm          AlgorithmIdentifier,
        subjectPublicKey   BIT_STRING
    }

    The encodings for the subjectPublicKey field are specified in the description of the public key types.
     */
    fn CKA_PUBLIC_KEY_INFO(&mut self) -> &mut ByteArray;
}

/** Private key objects (object class CKO_PRIVATE_KEY) hold private keys.

It is intended in the interests of interoperability that the subject name and key identifier for a private key will be the same as those for the corresponding certificate and public key. However, this is not enforced by Cryptoki, and it is not required that the certificate and public key also be stored on the token.

If the CKA_SENSITIVE attribute is CK_TRUE, or if the CKA_EXTRACTABLE attribute is CK_FALSE, then certain attributes of the private key cannot be revealed in plaintext outside the token. Which attributes these are is specified for each type of private key in the attribute table in the section describing that type of key.

The CKA_ALWAYS_AUTHENTICATE attribute can be used to force re-authentication (i.e. force the user to provide a PIN) for each use of a private key. “Use” in this case means a cryptographic operation such as sign or decrypt. This attribute may only be set to CK_TRUE when CKA_PRIVATE is also CK_TRUE.

Re-authentication occurs by calling C_Login with userType set to CKU_CONTEXT_SPECIFIC immediately after a cryptographic operation using the key has been initiated (e.g. after C_SignInit). In this call, the actual user type is implicitly given by the usage requirements of the active key. If C_Login returns CKR_OK the user was successfully authenticated and this sets the active key in an authenticated state that lasts until the cryptographic operation has successfully or unsuccessfully been completed (e.g. by C_Sign, C_SignFinal,..). A return value CKR_PIN_INCORRECT from C_Login means that the user was denied permission to use the key and continuing the cryptographic operation will result in a behavior as if C_Login had not been called. In both of these cases the session state will remain the same, however repeated failed re-authentication attempts may cause the PIN to be locked. C_Login returns in this case CKR_PIN_LOCKED and this also logs the user out from the token. Failing or omitting to re-1601 authenticate when CKA_ALWAYS_AUTHENTICATE is set to CK_TRUE will result in CKR_USER_NOT_LOGGED_IN to be returned from calls using the key. C_Login will return CKR_OPERATION_NOT_INITIALIZED, but the active cryptographic operation will not be affected, if an attempt is made to re-authenticate when CKA_ALWAYS_AUTHENTICATE is set to CK_FALSE.

The CKA_PUBLIC_KEY_INFO attribute represents the public key associated with this private key. The data it represents may either be stored as part of the private key data, or regenerated as needed from the private key.

If this attribute is supplied as part of a template for C_CreateObject, C_CopyObject or C_SetAttributeValue for a private key, the token MUST verify correspondence between the private key data and the public key data as supplied in CKA_PUBLIC_KEY_INFO. This can be done either by deriving a public key from the private key and comparing the values, or by doing a sign and verify operation. If there is a mismatch, the command SHALL return CKR_ATTRIBUTE_VALUE_INVALID. A token MAY choose not to support the CKA_PUBLIC_KEY_INFO attribute for commands which create new private keys. If it does not support the attribute, the command SHALL return CKR_ATTRIBUTE_TYPE_INVALID.

As a general guideline, private keys of any type SHOULD store sufficient information to retrieve the public key information. In particular, the RSA private key description has been modified in <this version> to add the CKA_PUBLIC_EXPONENT to the list of attributes required for an RSA private key. All other private key types described in this specification contain sufficient information to recover the associated public key.
*/
#[allow(non_snake_case)]
pub trait PrivateKeyObject: KeyObject {
    /// DER-encoding of certificate subject name (default empty)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_SUBJECT(&self) -> &ByteArray;
    /// CK_TRUE if key is sensitive
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SENSITIVE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports decryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_DECRYPT(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports signatures where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports signatures where the data can be recovered from the signature
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN_RECOVER(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports unwrapping (i.e., can be used to unwrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_UNWRAP(&self) -> &CK_BBOOL;
    /// CK_TRUE if key is extractable and can be wrapped
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_EXTRACTABLE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key has always had the CKA_SENSITIVE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_ALWAYS_SENSITIVE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key has never had the CKA_EXTRACTABLE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_NEVER_EXTRACTABLE(&self) -> &CK_BBOOL;
    /// CK_TRUE if the key can only be wrapped with a wrapping key that has CKA_TRUSTED set to CK_TRUE. Default is CK_FALSE.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_WRAP_WITH_TRUSTED(&self) -> &CK_BBOOL;
    /// For wrapping keys. The attribute template to apply to any keys unwrapped using this wrapping key. Any user supplied template is applied after this template as if the object has already been created. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_UNWRAP_TEMPLATE(&self) -> &CK_ATTRIBUTE_PTR;
    /// If CK_TRUE, the user has to supply the PIN for each use (sign or decrypt) with the key. Default is CK_FALSE.
    fn CKA_ALWAYS_AUTHENTICATE(&self) -> &CK_BBOOL;
    /// DER-encoding of the SubjectPublicKeyInfo for the associated public key (MAY be empty; DEFAULT derived from the underlying private key data; MAY be manually set for specific key types; if set; MUST be consistent with the underlying private key data)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_PUBLIC_KEY_INFO(&self) -> &ByteArray;
}

/** Private key objects (object class CKO_PRIVATE_KEY) hold private keys.

It is intended in the interests of interoperability that the subject name and key identifier for a private key will be the same as those for the corresponding certificate and public key. However, this is not enforced by Cryptoki, and it is not required that the certificate and public key also be stored on the token.

If the CKA_SENSITIVE attribute is CK_TRUE, or if the CKA_EXTRACTABLE attribute is CK_FALSE, then certain attributes of the private key cannot be revealed in plaintext outside the token. Which attributes these are is specified for each type of private key in the attribute table in the section describing that type of key.

The CKA_ALWAYS_AUTHENTICATE attribute can be used to force re-authentication (i.e. force the user to provide a PIN) for each use of a private key. “Use” in this case means a cryptographic operation such as sign or decrypt. This attribute may only be set to CK_TRUE when CKA_PRIVATE is also CK_TRUE.

Re-authentication occurs by calling C_Login with userType set to CKU_CONTEXT_SPECIFIC immediately after a cryptographic operation using the key has been initiated (e.g. after C_SignInit). In this call, the actual user type is implicitly given by the usage requirements of the active key. If C_Login returns CKR_OK the user was successfully authenticated and this sets the active key in an authenticated state that lasts until the cryptographic operation has successfully or unsuccessfully been completed (e.g. by C_Sign, C_SignFinal,..). A return value CKR_PIN_INCORRECT from C_Login means that the user was denied permission to use the key and continuing the cryptographic operation will result in a behavior as if C_Login had not been called. In both of these cases the session state will remain the same, however repeated failed re-authentication attempts may cause the PIN to be locked. C_Login returns in this case CKR_PIN_LOCKED and this also logs the user out from the token. Failing or omitting to re-1601 authenticate when CKA_ALWAYS_AUTHENTICATE is set to CK_TRUE will result in CKR_USER_NOT_LOGGED_IN to be returned from calls using the key. C_Login will return CKR_OPERATION_NOT_INITIALIZED, but the active cryptographic operation will not be affected, if an attempt is made to re-authenticate when CKA_ALWAYS_AUTHENTICATE is set to CK_FALSE.

The CKA_PUBLIC_KEY_INFO attribute represents the public key associated with this private key. The data it represents may either be stored as part of the private key data, or regenerated as needed from the private key.

If this attribute is supplied as part of a template for C_CreateObject, C_CopyObject or C_SetAttributeValue for a private key, the token MUST verify correspondence between the private key data and the public key data as supplied in CKA_PUBLIC_KEY_INFO. This can be done either by deriving a public key from the private key and comparing the values, or by doing a sign and verify operation. If there is a mismatch, the command SHALL return CKR_ATTRIBUTE_VALUE_INVALID. A token MAY choose not to support the CKA_PUBLIC_KEY_INFO attribute for commands which create new private keys. If it does not support the attribute, the command SHALL return CKR_ATTRIBUTE_TYPE_INVALID.

As a general guideline, private keys of any type SHOULD store sufficient information to retrieve the public key information. In particular, the RSA private key description has been modified in <this version> to add the CKA_PUBLIC_EXPONENT to the list of attributes required for an RSA private key. All other private key types described in this specification contain sufficient information to recover the associated public key.
*/
#[allow(non_snake_case)]
pub trait MutPrivateKeyObject: MutKeyObject {
    /// DER-encoding of certificate subject name (default empty)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_SUBJECT(&mut self) -> &mut ByteArray;
    /// CK_TRUE if key is sensitive
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SENSITIVE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports decryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_DECRYPT(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports signatures where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports signatures where the data can be recovered from the signature
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN_RECOVER(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports unwrapping (i.e., can be used to unwrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_UNWRAP(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key is extractable and can be wrapped
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_EXTRACTABLE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key has always had the CKA_SENSITIVE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_ALWAYS_SENSITIVE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key has never had the CKA_EXTRACTABLE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_NEVER_EXTRACTABLE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if the key can only be wrapped with a wrapping key that has CKA_TRUSTED set to CK_TRUE. Default is CK_FALSE.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_WRAP_WITH_TRUSTED(&mut self) -> &mut CK_BBOOL;
    /// For wrapping keys. The attribute template to apply to any keys unwrapped using this wrapping key. Any user supplied template is applied after this template as if the object has already been created. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_UNWRAP_TEMPLATE(&mut self) -> &mut CK_ATTRIBUTE_PTR;
    /// If CK_TRUE, the user has to supply the PIN for each use (sign or decrypt) with the key. Default is CK_FALSE.
    fn CKA_ALWAYS_AUTHENTICATE(&mut self) -> &mut CK_BBOOL;
    /// DER-encoding of the SubjectPublicKeyInfo for the associated public key (MAY be empty; DEFAULT derived from the underlying private key data; MAY be manually set for specific key types; if set; MUST be consistent with the underlying private key data)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    fn CKA_PUBLIC_KEY_INFO(&mut self) -> &mut ByteArray;
}

/** Secret key objects (object class CKO_SECRET_KEY) hold secret keys.

If the CKA_SENSITIVE attribute is CK_TRUE, or if the CKA_EXTRACTABLE attribute is CK_FALSE, then certain attributes of the secret key cannot be revealed in plaintext outside the token. Which attributes these are is specified for each type of secret key in the attribute table in the section describing that type of key.

The key check value (KCV) attribute for symmetric key objects to be called CKA_CHECK_VALUE, of type byte array, length 3 bytes, operates like a fingerprint, or checksum of the key. They are intended to be used to cross-check symmetric keys against other systems where the same key is shared, and as a validity check after manual key entry or restore from backup. Refer to object definitions of specific key types for KCV algorithms.

Properties:

1. For two keys that are cryptographically identical the value of this attribute should be identical.
2. CKA_CHECK_VALUE should not be usable to obtain any part of the key value.
3. Non-uniqueness. Two different keys can have the same CKA_CHECK_VALUE. This is unlikely (the probability can easily be calculated) but possible.

The attribute is optional, but if supported, regardless of how the key object is created or derived, the value of the attribute is always supplied. It SHALL be supplied even if the encryption operation for the key is forbidden (i.e. when CKA_ENCRYPT is set to CK_FALSE).

If a value is supplied in the application template (allowed but never necessary) then, if supported, it MUST match what the library calculates it to be or the library returns a CKR_ATTRIBUTE_VALUE_INVALID. If the library does not support the attribute then it should ignore it. Allowing the attribute in the template this way does no harm and allows the attribute to be treated like any other attribute for the purposes of key wrap and unwrap where the attributes are preserved also.

The generation of the KCV may be prevented by the application supplying the attribute in the template as a no-value (0 length) entry. The application can query the value at any time like any other attribute using C_GetAttributeValue. C_SetAttributeValue may be used to destroy the attribute, by supplying no-value.

Unless otherwise specified for the object definition, the value of this attribute is derived from the key object by taking the first three bytes of an encryption of a single block of null (0x00) bytes, using the default cipher and mode (e.g. ECB) associated with the key type of the secret key object.
 */
#[allow(non_snake_case)]
pub trait SecretKeyObject: KeyObject {
    /// CK_TRUE if object is sensitive (default CK_FALSE)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_SENSITIVE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports encryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_ENCRYPT(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports decryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_DECRYPT(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports signatures (i.e., authentication codes) where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports verification (i.e., of authentication codes) where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports wrapping (i.e., can be used to wrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_WRAP(&self) -> &CK_BBOOL;
    /// CK_TRUE if key supports unwrapping (i.e., can be used to unwrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_UNWRAP(&self) -> &CK_BBOOL;
    /// CK_TRUE if key is extractable and can be wrapped
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_FALSE. It becomes a read only attribute.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_EXTRACTABLE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key has always had the CKA_SENSITIVE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_ALWAYS_SENSITIVE(&self) -> &CK_BBOOL;
    /// CK_TRUE if key has never had the CKA_EXTRACTABLE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_NEVER_EXTRACTABLE(&self) -> &CK_BBOOL;
    /// Key checksum
    fn CKA_CHECK_VALUE(&self) -> &ByteArray;
    /// CK_TRUE if the key can only be wrapped with a wrapping key that has CKA_TRUSTED set to CK_TRUE. Default is CK_FALSE.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_WRAP_WITH_TRUSTED(&self) -> &CK_BBOOL;
    /// The wrapping key can be used to wrap keys with CKA_WRAP_WITH_TRUSTED set to CK_TRUE.
    ///
    /// Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&self) -> &CK_BBOOL;
    /// For wrapping keys. The attribute template to match against any keys wrapped using this wrapping key. Keys that do not match cannot be wrapped. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE
    fn CKA_WRAP_TEMPLATE(&self) -> &CK_ATTRIBUTE_PTR;
    /// For wrapping keys. The attribute template to apply to any keys unwrapped using this wrapping key. Any user supplied template is applied after this template as if the object has already been created. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_UNWRAP_TEMPLATE(&self) -> &CK_ATTRIBUTE_PTR;
}

/** Secret key objects (object class CKO_SECRET_KEY) hold secret keys.

If the CKA_SENSITIVE attribute is CK_TRUE, or if the CKA_EXTRACTABLE attribute is CK_FALSE, then certain attributes of the secret key cannot be revealed in plaintext outside the token. Which attributes these are is specified for each type of secret key in the attribute table in the section describing that type of key.

The key check value (KCV) attribute for symmetric key objects to be called CKA_CHECK_VALUE, of type byte array, length 3 bytes, operates like a fingerprint, or checksum of the key. They are intended to be used to cross-check symmetric keys against other systems where the same key is shared, and as a validity check after manual key entry or restore from backup. Refer to object definitions of specific key types for KCV algorithms.

Properties:

1. For two keys that are cryptographically identical the value of this attribute should be identical.
2. CKA_CHECK_VALUE should not be usable to obtain any part of the key value.
3. Non-uniqueness. Two different keys can have the same CKA_CHECK_VALUE. This is unlikely (the probability can easily be calculated) but possible.

The attribute is optional, but if supported, regardless of how the key object is created or derived, the value of the attribute is always supplied. It SHALL be supplied even if the encryption operation for the key is forbidden (i.e. when CKA_ENCRYPT is set to CK_FALSE).

If a value is supplied in the application template (allowed but never necessary) then, if supported, it MUST match what the library calculates it to be or the library returns a CKR_ATTRIBUTE_VALUE_INVALID. If the library does not support the attribute then it should ignore it. Allowing the attribute in the template this way does no harm and allows the attribute to be treated like any other attribute for the purposes of key wrap and unwrap where the attributes are preserved also.

The generation of the KCV may be prevented by the application supplying the attribute in the template as a no-value (0 length) entry. The application can query the value at any time like any other attribute using C_GetAttributeValue. C_SetAttributeValue may be used to destroy the attribute, by supplying no-value.

Unless otherwise specified for the object definition, the value of this attribute is derived from the key object by taking the first three bytes of an encryption of a single block of null (0x00) bytes, using the default cipher and mode (e.g. ECB) associated with the key type of the secret key object.
 */
#[allow(non_snake_case)]
pub trait MutSecretKeyObject: MutKeyObject {
    /// CK_TRUE if object is sensitive (default CK_FALSE)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_SENSITIVE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports encryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_ENCRYPT(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports decryption
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_DECRYPT(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports signatures (i.e., authentication codes) where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_SIGN(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports verification (i.e., of authentication codes) where the signature is an appendix to the data
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_VERIFY(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports wrapping (i.e., can be used to wrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_WRAP(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key supports unwrapping (i.e., can be used to unwrap other keys)
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_UNWRAP(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key is extractable and can be wrapped
    ///
    /// May be modified after object is created with a C_SetAttributeValue call, or in the process of copying object with a C_CopyObject call. However, it is possible that a particular token may not permit modification of the attribute during the course of a C_CopyObject call.
    ///
    /// Attribute cannot be changed once set to CK_FALSE. It becomes a read only attribute.
    ///
    /// Default value is token-specific, and may depend on the values of other attributes.
    fn CKA_EXTRACTABLE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key has always had the CKA_SENSITIVE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_ALWAYS_SENSITIVE(&mut self) -> &mut CK_BBOOL;
    /// CK_TRUE if key has never had the CKA_EXTRACTABLE attribute set to CK_TRUE
    ///
    /// MUST not be specified when object is created with C_CreateObject.
    ///
    /// MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    ///
    /// MUST not be specified when object is unwrapped with C_UnwrapKey.
    fn CKA_NEVER_EXTRACTABLE(&mut self) -> &mut CK_BBOOL;
    /// Key checksum
    fn CKA_CHECK_VALUE(&mut self) -> &mut ByteArray;
    /// CK_TRUE if the key can only be wrapped with a wrapping key that has CKA_TRUSTED set to CK_TRUE. Default is CK_FALSE.
    ///
    /// Attribute cannot be changed once set to CK_TRUE. It becomes a read only attribute.
    fn CKA_WRAP_WITH_TRUSTED(&mut self) -> &mut CK_BBOOL;
    /// The wrapping key can be used to wrap keys with CKA_WRAP_WITH_TRUSTED set to CK_TRUE.
    ///
    /// Can only be set to CK_TRUE by the SO user.
    fn CKA_TRUSTED(&mut self) -> &mut CK_BBOOL;
    /// For wrapping keys. The attribute template to match against any keys wrapped using this wrapping key. Keys that do not match cannot be wrapped. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE
    fn CKA_WRAP_TEMPLATE(&mut self) -> &mut CK_ATTRIBUTE_PTR;
    /// For wrapping keys. The attribute template to apply to any keys unwrapped using this wrapping key. Any user supplied template is applied after this template as if the object has already been created. The number of attributes in the array is the ulValueLen component of the attribute divided by the size of CK_ATTRIBUTE.
    fn CKA_UNWRAP_TEMPLATE(&mut self) -> &mut CK_ATTRIBUTE_PTR;
}

/** This object class was created to support the storage of certain algorithm's extended parameters. DSA and DH both use domain parameters in the key-pair generation step. In particular, some libraries support the generation of domain parameters (originally out of scope for PKCS11) so the object class was added.

To use a domain parameter object you MUST extract the attributes into a template and supply them (still in the template) to the corresponding key-pair generation function.

Domain parameter objects (object class CKO_DOMAIN_PARAMETERS) hold public domain parameters.

The CKA_LOCAL attribute has the value CK_TRUE if and only if the values of the domain parameters were originally generated on the token by a C_GenerateKey call.
 */
#[allow(non_snake_case)]
pub trait DomainParameterObject: StorageObject {
    /// Type of key the domain parameters can be used to generate.
    ///
    /// MUST be specified when object is created with C_CreateObject.
    fn CKA_KEY_TYPE(&self) -> &CK_KEY_TYPE;
    /**
    CK_TRUE only if domain parameters were either

    * generated locally (i.e., on the token) with a C_GenerateKey
    * created with a C_CopyObject call as a copy of domain parameters which had its CKA_LOCAL attribute set to CK_TRUE

    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    */
    fn CKA_LOCAL(&self) -> &CK_BBOOL;
}

/** This object class was created to support the storage of certain algorithm's extended parameters. DSA and DH both use domain parameters in the key-pair generation step. In particular, some libraries support the generation of domain parameters (originally out of scope for PKCS11) so the object class was added.

To use a domain parameter object you MUST extract the attributes into a template and supply them (still in the template) to the corresponding key-pair generation function.

Domain parameter objects (object class CKO_DOMAIN_PARAMETERS) hold public domain parameters.

The CKA_LOCAL attribute has the value CK_TRUE if and only if the values of the domain parameters were originally generated on the token by a C_GenerateKey call.
 */
#[allow(non_snake_case)]
pub trait MutDomainParameterObject: MutStorageObject {
    /// Type of key the domain parameters can be used to generate.
    ///
    /// MUST be specified when object is created with C_CreateObject.
    fn CKA_KEY_TYPE(&mut self) -> &mut CK_KEY_TYPE;
    /**
    CK_TRUE only if domain parameters were either

    * generated locally (i.e., on the token) with a C_GenerateKey
    * created with a C_CopyObject call as a copy of domain parameters which had its CKA_LOCAL attribute set to CK_TRUE

    MUST not be specified when object is created with C_CreateObject.

    MUST not be specified when object is generated with C_GenerateKey or C_GenerateKeyPair.
    */
    fn CKA_LOCAL(&mut self) -> &mut CK_BBOOL;
}

/** Mechanism objects provide information about mechanisms supported by a device beyond that given by the CK_MECHANISM_INFO structure.

When searching for objects using C_FindObjectsInit and C_FindObjects, mechanism objects are not returned unless the CKA_CLASS attribute in the template has the value CKO_MECHANISM. This protects applications written to previous versions of Cryptoki from finding objects that they do not understand.

The CKA_MECHANISM_TYPE attribute may not be set.
*/
#[allow(non_snake_case)]
pub trait MechanismObject: BaseObject {
    /// The type of mechanism object
    fn CKA_MECHANISM_TYPE(&self) -> &CK_MECHANISM_TYPE;
}

/** Mechanism objects provide information about mechanisms supported by a device beyond that given by the CK_MECHANISM_INFO structure.

When searching for objects using C_FindObjectsInit and C_FindObjects, mechanism objects are not returned unless the CKA_CLASS attribute in the template has the value CKO_MECHANISM. This protects applications written to previous versions of Cryptoki from finding objects that they do not understand.

The CKA_MECHANISM_TYPE attribute may not be set.
*/
#[allow(non_snake_case)]
pub trait MutMechanismObject: MutBaseObject {
    /// The type of mechanism object
    fn CKA_MECHANISM_TYPE(&mut self) -> &mut CK_MECHANISM_TYPE;
}

/** Profile objects (object class CKO_PROFILE) describe which PKCS #11 profiles the token implements. Profiles are defined in the OASIS PKCS #11 Cryptographic Token Interface Profiles document. A given token can contain more than one profile ID.

The CKA_PROFILE_ID attribute identifies a profile that the token supports.
*/
#[allow(non_snake_case)]
pub trait ProfileObject: BaseObject {
    /// ID of the supported profile.
    fn CKA_PROFILE_ID(&self) -> &CK_PROFILE_ID;
}

/** Profile objects (object class CKO_PROFILE) describe which PKCS #11 profiles the token implements. Profiles are defined in the OASIS PKCS #11 Cryptographic Token Interface Profiles document. A given token can contain more than one profile ID.

The CKA_PROFILE_ID attribute identifies a profile that the token supports.
*/
#[allow(non_snake_case)]
pub trait MutProfileObject: MutBaseObject {
    /// ID of the supported profile.
    fn CKA_PROFILE_ID(&mut self) -> &mut CK_PROFILE_ID;
}
