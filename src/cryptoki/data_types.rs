use std::ffi::c_void;

pub mod mechanisms;
pub mod objects;

/*
A deliberate decision has been made here to deviate from

Prefix	| Description
--------+---------------
C_		| Function
CK_		| Data type or general constant
CKA_	| Attribute
CKC_	| Certificate type
CKD_	| Key derivation function
CKF_	| Bit flag
CKG_	| Mask generation function
CKH_	| Hardware feature type
CKK_	| Key type
CKM_	| Mechanism type
CKN_	| Notification
CKO_	| Object class
CKP_	| Pseudo-random function
CKS_	| Session state
CKR_	| Return value
CKU_	| User type
CKZ_	| Salt/Encoding parameter source
h		| a handle
ul		| a CK_ULONG
p		| a pointer
pb		| a pointer to a CK_BYTE
ph		| a pointer to a handle
pul		| a pointer to a CK_ULONG
*/

/* CK_CHAR charset:
Letters
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z
Numbers
0 1 2 3 4 5 6 7 8 9
Graphic characters
! “ # % & ‘ ( ) * + , - . / : ; < = > ? [ \ ] ^ _ { | } ~
Blank character
' ' (space)
*/

// Local functions
#[allow(non_camel_case_types)]
pub type CK_DECLARE_FUNCTION<returnType> = fn() -> returnType;
#[allow(non_camel_case_types)]
pub type CK_DECLARE_FUNCTION_POINTER<returnType> = *const CK_DECLARE_FUNCTION<returnType>;
#[allow(non_camel_case_types)]
pub type CK_DECLARE_FUNCTION_1<returnType, arg1> = fn(_: arg1) -> returnType;
#[allow(non_camel_case_types)]
pub type CK_DECLARE_FUNCTION_1_POINTER<returnType, arg1> =
    *const CK_DECLARE_FUNCTION_1<returnType, arg1>;

// Callback functions in other applications
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION<returnType> = extern "C" fn() -> returnType;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_POINTER<returnType> = *const CK_CALLBACK_FUNCTION<returnType>;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_1<returnType, arg1> = extern "C" fn(_: arg1) -> returnType;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_1_POINTER<returnType, arg1> =
    *const CK_CALLBACK_FUNCTION_1<returnType, arg1>;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_2<returnType, arg1, arg2> =
    extern "C" fn(_: arg1, _: arg2) -> returnType;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_2_POINTER<returnType, arg1, arg2> =
    *const CK_CALLBACK_FUNCTION_2<returnType, arg1, arg2>;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_3<returnType, arg1, arg2, arg3> =
    extern "C" fn(_: arg1, _: arg2, _: arg3) -> returnType;
#[allow(non_camel_case_types)]
pub type CK_CALLBACK_FUNCTION_3_POINTER<returnType, arg1, arg2, arg3> =
    *const CK_CALLBACK_FUNCTION_3<returnType, arg1, arg2, arg3>;

/// The Cryptoki version of this library
pub const LIB_CK_VERSION: CK_VERSION = CK_VERSION { major: 3, minor: 0 };
/// A pointer to the Cryptoki version of this library
pub const LIB_CK_VERSION_PTR: CK_VERSION_PTR = &LIB_CK_VERSION;

// Basic types
#[allow(non_camel_case_types)]
pub type CK_BYTE = u8;
#[allow(non_camel_case_types)]
pub type CK_CHAR = CK_BYTE;
#[allow(non_camel_case_types)]
pub type CK_UTF8CHAR = CK_BYTE;
#[allow(non_camel_case_types)]
pub type CK_BBOOL = CK_BYTE;
#[allow(non_camel_case_types)]
pub type CK_ULONG = usize;
#[allow(non_camel_case_types)]
pub type CK_LONG = isize;
#[allow(non_camel_case_types)]
pub type CK_FLAGS = CK_ULONG;
#[allow(non_camel_case_types)]
pub type CK_VOID = *const c_void;

/// True
pub const CK_TRUE: CK_BBOOL = 1;
/// False
pub const CK_FALSE: CK_BBOOL = 0;

// Basic pointer types
#[allow(non_camel_case_types)]
pub type CK_BYTE_PTR = *const CK_BYTE;
#[allow(non_camel_case_types)]
pub type CK_CHAR_PTR = *const CK_CHAR;
#[allow(non_camel_case_types)]
pub type CK_UTF8CHAR_PTR = *const CK_UTF8CHAR;
#[allow(non_camel_case_types)]
pub type CK_ULONG_PTR = *const CK_ULONG;

/// CK_VERSION is a structure that describes the version of a Cryptoki interface, a Cryptoki library, or an SSL or TLS implementation, or the hardware or firmware version of a slot or token
#[repr(C)]
pub struct CK_VERSION {
    /// Major version number (the integer portion of the version)
    major: CK_BYTE,
    /// Minor version number (the hundredths portion of the version)
    minor: CK_BYTE,
}

#[allow(non_camel_case_types)]
pub type CK_VERSION_PTR = *const CK_VERSION;

/// CK_INFO provides general information about Cryptoki
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_INFO {
    /// Cryptoki interface version number, for compatibility with future revisions of this interface
    pub cryptokiVersion: CK_VERSION,
    /// ID of the Cryptoki library manufacturer. MUST be padded with the blank character (‘ ‘). Should not be null-terminated
    pub manufacturerID: [CK_UTF8CHAR; 32],
    /// bit flags reserved for future versions. MUST be zero for this version
    pub flags: CK_FLAGS,
    /// character-string description of the library. MUST be padded with the blank character (‘ ‘). Should not be null-terminated.
    pub libraryDescription: [CK_UTF8CHAR; 32],
    /// Cryptoki library version number
    pub libraryVersion: CK_VERSION,
}

#[allow(non_camel_case_types)]
pub type CK_INFO_PTR = *const CK_INFO;

/// CK_NOTIFICATION holds the types of notifications that Cryptoki provides to an application.
#[allow(non_camel_case_types)]
pub type CK_NOTIFICATION = CK_ULONG;

/// CK_SLOT_ID is a Cryptoki-assigned value that identifies a slot.
#[allow(non_camel_case_types)]
pub type CK_SLOT_ID = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_SLOT_ID_PTR = *const CK_SLOT_ID;

/// CK_SLOT_INFO provides information about a slot
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_SLOT_INFO {
    /// character-string description of the slot. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub slotDescription: [CK_UTF8CHAR; 64],
    /// ID of the slot manufacturer. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub manufacturerID: [CK_UTF8CHAR; 32],
    /** bits flags that provide capabilities of the slot. The flags are defined below

    Bit Flag				| Mask			| Meaning
    ------------------------|---------------|--------------------------------------------------------------------------
    CKF_TOKEN_PRESENT		| 0x00000001	| True if a token is present in the slot (e.g. a device is in the reader)
    CKF_REMOVABLE_DEVICE	| 0x00000002	| True if the reader supports removable devices
    CKF_HW_SLOT				| 0x00000004	| True if the slot is a hardware slot, as opposed to a software slot implementing a “soft token”

    For a given slot, the value of the CKF_REMOVABLE_DEVICE flag never changes. In addition, if this flag is not set for a given slot, then the CKF_TOKEN_PRESENT flag for that slot is always set. That is, if a slot does not support a removable device, then that slot always has a token in it.
    */
    pub flags: CK_FLAGS,
    /// version number of the slot’s hardware
    pub hardwareVersion: CK_VERSION,
    /// version number of the slot’s firmware
    pub firmwareVersion: CK_VERSION,
}

#[allow(non_camel_case_types)]
pub type CK_SLOT_INFO_PTR = *const CK_SLOT_INFO;

/** CK_TOKEN_INFO provides information about a token.

Exactly what the CKF_WRITE_PROTECTED flag means is not specified in Cryptoki. An application may be unable to perform certain actions on a write-protected token; these actions can include any of the following, among others:

* Creating/modifying/deleting any object on the token.
* Creating/modifying/deleting a token object on the token.
* Changing the SO’s PIN.
* Changing the normal user’s PIN.

The token may change the value of the **CKF_WRITE_PROTECTED** flag depending on the session state to implement its object management policy. For instance, the token may set the **CKF_WRITE_PROTECTED** flag unless the session state is R/W SO or R/W User to implement a policy that does not allow any objects, public or private, to be created, modified, or deleted unless the user has successfully called C_Login.

The **CKF_USER_PIN_COUNT_LOW**, **CKF_USER_PIN_COUNT_LOW**, **CKF_USER_PIN_FINAL_TRY**, and **CKF_SO_PIN_FINAL_TRY** flags may always be set to false if the token does not support the functionality or will not reveal the information because of its security policy.

The **CKF_USER_PIN_TO_BE_CHANGED** and **CKF_SO_PIN_TO_BE_CHANGED** flags may always be set to false if the token does not support the functionality. If a PIN is set to the default value, or has expired, the appropriate **CKF_USER_PIN_TO_BE_CHANGED** or **CKF_SO_PIN_TO_BE_CHANGED** flag is set to true. When either of these flags are true, logging in with the corresponding PIN will succeed, but only the C_SetPIN function can be called. Calling any other function that required the user to be logged in will cause CKR_PIN_EXPIRED to be returned until C_SetPIN is called successfully.

**CK_TOKEN_INFO Note**: The fields ulMaxSessionCount, ulSessionCount, ulMaxRwSessionCount, ulRwSessionCount, ulTotalPublicMemory, ulFreePublicMemory, ulTotalPrivateMemory, and ulFreePrivateMemory can have the special value CK_UNAVAILABLE_INFORMATION, which means that the token and/or library is unable or unwilling to provide that information. In addition, the fields ulMaxSessionCount and ulMaxRwSessionCount can have the special value CK_EFFECTIVELY_INFINITE, which means that there is no practical limit on the number of sessions (resp. R/W sessions) an application can have open with the token.

It is important to check these fields for these special values. This is particularly true for CK_EFFECTIVELY_INFINITE, since an application seeing this value in the ulMaxSessionCount or ulMaxRwSessionCount field would otherwise conclude that it can’t open any sessions with the token, which is far from being the case.

The upshot of all this is that the correct way to interpret (for example) the ulMaxSessionCount field is something along the lines of the following:

```C
CK_TOKEN_INFO info;
.
.
if ((CK_LONG) info.ulMaxSessionCount
    == CK_UNAVAILABLE_INFORMATION) {
  /* Token refuses to give value of ulMaxSessionCount */
  .
  .
} else if (info.ulMaxSessionCount == CK_EFFECTIVELY_INFINITE) {
  /* Application can open as many sessions as it wants */
  .
  .
} else {
  /* ulMaxSessionCount really does contain what it should */
  .
  .
}
```
*/
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_TOKEN_INFO {
    /// application-defined label, assigned during token initialization. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub label: [CK_UTF8CHAR; 32],
    /// ID of the device manufacturer. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub manufacturerID: [CK_UTF8CHAR; 32],
    /// model of the device. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub model: [CK_UTF8CHAR; 16],
    /// character-string serial number of the device. MUST be padded with the blank character (‘ ‘). MUST NOT be null-terminated.
    pub serialNumber: [CK_UTF8CHAR; 16],
    /** bit flags indicating capabilities and status of the device as defined below

    Bit Flag							| Mask			| Meaning
    ------------------------------------|---------------|--------------------------------------------------------------------------
    CKF_RNG								| 0x00000001	| True if the token has its own random number generator
    CKF_WRITE_PROTECTED					| 0x00000002	| True if the token is write-protected (see below)
    CKF_LOGIN_REQUIRED					| 0x00000004	| True if there are some cryptographic functions that a user MUST be logged in to perform
    CKF_USER_PIN_INITIALIZED			| 0x00000008	| True if the normal user’s PIN has been initialized
    CKF_RESTORE_KEY_NOT_NEEDED			| 0x00000020	| True if a successful save of a session’s cryptographic operations state always contains all keys needed to restore the state of the session
    CKF_CLOCK_ON_TOKEN					| 0x00000040	| True if token has its own hardware clock
    CKF_PROTECTED_AUTHENTICATION_PATH	| 0x00000100	| True if token has a “protected authentication path”, whereby a user can log into the token without passing a PIN through the Cryptoki library
    CKF_DUAL_CRYPTO_OPERATIONS			| 0x00000200	| True if a single session with the token can perform dual cryptographic operations (see Section 5.14)
    CKF_TOKEN_INITIALIZED				| 0x00000400	| True if the token has been initialized using C_InitToken or an equivalent mechanism outside the scope of this standard. Calling C_InitToken when this flag is set will cause the token to be reinitialized.
    CKF_SECONDARY_AUTHENTICATION		| 0x00000800	| True if the token supports secondary authentication for private key objects. (Deprecated; new implementations MUST NOT set this flag)
    CKF_USER_PIN_COUNT_LOW				| 0x00010000	| True if an incorrect user login PIN has been entered at least once since the last successful authentication.
    CKF_USER_PIN_FINAL_TRY				| 0x00020000	| True if supplying an incorrect user PIN will cause it to become locked.
    CKF_USER_PIN_LOCKED					| 0x00040000	| True if the user PIN has been locked. User login to the token is not possible.
    CKF_USER_PIN_TO_BE_CHANGED			| 0x00080000	| True if the user PIN value is the default value set by token initialization or manufacturing, or the PIN has been expired by the card.
    CKF_SO_PIN_COUNT_LOW				| 0x00100000	| True if an incorrect SO login PIN has been entered at least once since the last successful authentication.
    CKF_SO_PIN_FINAL_TRY				| 0x00200000	| True if supplying an incorrect SO PIN will cause it to become locked.
    CKF_SO_PIN_LOCKED					| 0x00400000	| True if the SO PIN has been locked. SO login to the token is not possible.
    CKF_SO_PIN_TO_BE_CHANGED			| 0x00800000	| True if the SO PIN value is the default value set by token initialization or manufacturing, or the PIN has been expired by the card.
    CKF_ERROR_STATE						| 0x01000000	| True if the token failed a FIPS-2 self-test and entered an error state.
    */
    pub flags: CK_FLAGS,
    ///  maximum number of sessions that can be opened with the token at one time by a single application (see CK_TOKEN_INFO definition for details)
    pub ulMaxSessionCount: CK_ULONG,
    /// number of sessions that this application currently has open with the token (see CK_TOKEN_INFO definition for details)
    pub ulSessionCount: CK_ULONG,
    /// maximum number of read/write sessions that can be opened with the token at one time by a single application (see CK_TOKEN_INFO definition for details)
    pub ulMaxRwSessionCount: CK_ULONG,
    /// number of read/write sessions that this application currently has open with the token (see CK_TOKEN_INFO definition for details)
    pub ulRwSessionCount: CK_ULONG,
    /// maximum length in bytes of the PIN
    pub ulMaxPinLen: CK_ULONG,
    /// minimum length in bytes of the PIN
    pub ulMinPinLen: CK_ULONG,
    /// the total amount of memory on the token in bytes in which public objects may be stored (see CK_TOKEN_INFO definition for details)
    pub ulTotalPublicMemory: CK_ULONG,
    /// the amount of free (unused) memory on the token in bytes for public objects (see CK_TOKEN_INFO definition for details)
    pub ulFreePublicMemory: CK_ULONG,
    /// the total amount of memory on the token in bytes in which private objects may be stored (see CK_TOKEN_INFO definition for details)
    pub ulTotalPrivateMemory: CK_ULONG,
    /// the amount of free (unused) memory on the token in bytes for private objects (see CK_TOKEN_INFO definition for details)
    pub ulFreePrivateMemory: CK_ULONG,
    /// version number of hardware
    pub hardwareVersion: CK_VERSION,
    /// version number of firmware
    pub firmwareVersion: CK_VERSION,
    /// current time as a character-string of length 16, represented in the format YYYYMMDDhhmmssxx (4 characters for the year; 2 characters each for the month, the day, the hour, the minute, and the second; and 2 additional reserved ‘0’ characters). The value of this field only makes sense for tokens equipped with a clock, as indicated in the token information flags (see below)
    pub uctTime: [CK_CHAR; 16],
}

#[allow(non_camel_case_types)]
pub type CK_TOKEN_INFO_PTR = *const CK_TOKEN_INFO;

/// CK_SESSION_HANDLE is a Cryptoki-assigned value that identifies a session. *Valid session handles in Cryptoki always have nonzero values.*
#[allow(non_camel_case_types)]
pub type CK_SESSION_HANDLE = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_SESSION_HANDLE_PTR = *const CK_SESSION_HANDLE;

/// CK_USER_TYPE holds the types of Cryptoki users described in [PKCS11-UG](https://docs.oasis-open.org/pkcs11/pkcs11-ug/v2.40/pkcs11-ug-v2.40.html) and, in addition, a context-specific type described in Section 4.9
#[allow(non_camel_case_types)]
pub type CK_USER_TYPE = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_USER_TYPE_PTR = *const CK_USER_TYPE;

// TODO: define the CKU constants, CKU_SO, CKU_USER, CKU_CONTEXT_SPECIFIC. The problem is that their values aren't defined anywhere I can find.

/// CK_STATE holds the session state, as described in [PKCS11-UG](https://docs.oasis-open.org/pkcs11/pkcs11-ug/v2.40/pkcs11-ug-v2.40.html).
/**
For this version of Cryptoki, the following session states are defined:
 CKS_RO_PUBLIC_SESSION
 CKS_RO_USER_FUNCTIONS
 CKS_RW_PUBLIC_SESSION
 CKS_RW_USER_FUNCTIONS
 CKS_RW_SO_FUNCTIONS
*/
#[allow(non_camel_case_types)]
pub type CK_STATE = CK_ULONG;

#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_SESSION_INFO {
    /// ID of the slot that interfaces with the token
    pub slotID: CK_SLOT_ID,
    /// the state of the session
    pub state: CK_STATE,
    /** bit flags that define the type of session; the flags are defined below

    Bit Flag				| Mask			| Meaning
    ------------------------|---------------|--------------------------------------------------------------------------
    CKF_RW_SESSION			| 0x00000002	| True if the session is read/write; false if the session is read-only
    CKF_SERIAL_SESSION		| 0x00000004	| This flag is provided for backward compatibility, and should always be set to true

    */
    pub flags: CK_FLAGS,
    /// an error code defined by the cryptographic device. Used for errors not covered by Cryptoki.
    pub ulDeviceError: CK_ULONG,
}

#[allow(non_camel_case_types)]
pub type CK_SESSION_INFO_PTR = *const CK_SESSION_INFO;

/** CK_OBJECT_HANDLE is a token-specific identifier for an object.

When an object is created or found on a token by an application, Cryptoki assigns it an object handle for
that application’s sessions to use to access it. A particular object on a token does not necessarily have a
handle which is fixed for the lifetime of the object; however, if a particular session can use a particular
handle to access a particular object, then that session will continue to be able to use that handle to
access that object as long as the session continues to exist, the object continues to exist, and the object
continues to be accessible to the session. *Valid object handles in Cryptoki always have nonzero values*
*/
#[allow(non_camel_case_types)]
pub type CK_OBJECT_HANDLE = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_OBJECT_HANDLE_PTR = *const CK_OBJECT_HANDLE;

/** CK_OBJECT_CLASS is a value that identifies the classes (or types) of objects that Cryptoki recognizes.

Object classes are defined with the objects that use them. The type is specified on an object through the CKA_CLASS attribute of the object.

Vendor defined values for this type may also be specified.
*/
#[allow(non_camel_case_types)]
pub type CK_OBJECT_CLASS = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_OBJECT_CLASS_PTR = *const CK_OBJECT_CLASS;

/**
CK_HW_FEATURE_TYPE is a value that identifies a hardware feature type of a device

Hardware feature types are defined with the objects that use them. The type is specified on an object through the CKA_HW_FEATURE_TYPE attribute of the object.

Vendor defined values for this type may also be specified. CKH_VENDOR_DEFINED

Feature types CKH_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their feature types through the PKCS process.
*/
#[allow(non_camel_case_types)]
pub type CK_HW_FEATURE_TYPE = CK_ULONG;

/**
CK_KEY_TYPE is a value that identifies a key type

Key types are defined with the objects and mechanisms that use them. The key type is specified on an object through the CKA_KEY_TYPE attribute of the object.

Vendor defined values for this type may also be specified. CKK_VENDOR_DEFINED

Key types CKK_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their key types through the PKCS process.

*/
#[allow(non_camel_case_types)]
pub type CK_KEY_TYPE = CK_ULONG;

/**
CK_CERTIFICATE_TYPE is a value that identifies a certificate type

Certificate types are defined with the objects and mechanisms that use them. The certificate type is specified on an object through the CKA_CERTIFICATE_TYPE attribute of the object.

Vendor defined values for this type may also be specified. CKC_VENDOR_DEFINED

Certificate types CKC_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their certificate types through the PKCS process.

*/
#[allow(non_camel_case_types)]
pub type CK_CERTIFICATE_TYPE = CK_ULONG;

/**
CK_CERTIFICATE_CATEGORY is a value that identifies a certificate category

For this version of Cryptoki, the following certificate categories are defined:

Constant								| Value			| Meaning
----------------------------------------|---------------|--------------------------------------------------------------------------
CK_CERTIFICATE_CATEGORY_UNSPECIFIED		| 0x00000000	| No category specified
CK_CERTIFICATE_CATEGORY_TOKEN_USER		| 0x00000001	| Certificate belongs to owner of the token
CK_CERTIFICATE_CATEGORY_AUTHORITY		| 0x00000002	| Certificate belongs to a certificate authority
CK_CERTIFICATE_CATEGORY_OTHER_ENTITY	| 0x00000003	| Certificate belongs to an end entity (i.e.: not a CA)

*/
#[allow(non_camel_case_types)]
pub type CK_CERTIFICATE_CATEGORY = CK_ULONG;

/**
CK_ATTRIBUTE_TYPE is a value that identifies an attribute type

Attributes are defined with the objects and mechanisms that use them. Attributes are specified on an object as a list of type, length value items. These are often specified as an attribute template.

Vendor defined values for this type may also be specified. CKA_VENDOR_DEFINED

Attribute types CKA_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their attribute types through the PKCS process.

*/
#[allow(non_camel_case_types)]
pub type CK_ATTRIBUTE_TYPE = CK_ULONG;

/**
CK_ATTRIBUTE is a structure that includes the type, value, and length of an attribute

If an attribute has no value, then ulValueLen = 0, and the value of pValue is irrelevant. An array of CK_ATTRIBUTEs is called a “template” and is used for creating, manipulating and searching for objects. The order of the attributes in a template never matters, even if the template contains vendor-specific attributes. Note that pValue is a “void” pointer, facilitating the passing of arbitrary values. Both the application and Cryptoki library MUST ensure that the pointer can be safely cast to the expected type (i.e., without word-alignment errors).

The constant CK_UNAVAILABLE_INFORMATION is used in the ulValueLen field to denote an invalid or unavailable value. See C_GetAttributeValue for further details.
*/
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_ATTRIBUTE {
    /// the attribute type. Note that "type" is an invalid property name in Rust, so we use attributeType
    pub attributeType: CK_ATTRIBUTE_TYPE,
    /// pointer to the value of the attribute
    pub pValue: CK_VOID,
    /// length in bytes of the value
    pub ulValueLen: CK_ULONG,
}

#[allow(non_camel_case_types)]
pub type CK_ATTRIBUTE_PTR = *const CK_ATTRIBUTE;

/**
CK_DATE is a structure that defines a date

The fields hold numeric characters from the character set in Table 3, not the literal byte values.

When a Cryptoki object carries an attribute of this type, and the default value of the attribute is specified to be "empty," then Cryptoki libraries SHALL set the attribute's ulValueLen to 0.

Note that implementations of previous versions of Cryptoki may have used other methods to identify an "empty" attribute of type CK_DATE, and applications that needs to interoperate with these libraries therefore have to be flexible in what they accept as an empty value.
*/
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_DATE {
    /// the year (“1900” - “9999”)
    pub year: [CK_CHAR; 4],
    /// the month (“01” - “12”)
    pub month: [CK_CHAR; 2],
    /// the day (“01” - “31”)
    pub day: [CK_CHAR; 2],
}

/**
CK_PROFILE_ID is an unsigend ulong value represting a specific token profile

Profiles are defines in the PKCS #11 Cryptographic Token Interface Profiles document. s. ID's greater than 0xffffffff may cause compatibility issues on platforms that have CK_ULONG values of 32 bits, and should be avoided.

Vendor defined values for this type may also be specified. CKP_VENDOR_DEFINED

Profile IDs CKP_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their object classes through the PKCS process.

Valid Profile IDs in Cryptoki always have nonzero values. For developers’ convenience, Cryptoki defines the following symbolic value:

*/
#[allow(non_camel_case_types)]
pub type CK_PROFILE_ID = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_PROFILE_ID_PTR = *const CK_PROFILE_ID;

/**
CK_JAVA_MIDP_SECURITY_DOMAIN is a value that identifies the Java MIDP security domain of a certificate

For this version of Cryptoki, the following security domains are defined. See the Java MIDP specification for further information:

Constant								| Value			| Meaning
----------------------------------------|---------------|--------------------------------------------------------------------------
CK_SECURITY_DOMAIN_UNSPECIFIED			| 0x00000000	| No domain specified
CK_SECURITY_DOMAIN_MANUFACTURER			| 0x00000001	| Manufacturer protection domain
CK_SECURITY_DOMAIN_OPERATOR				| 0x00000002	| Operator protection domain
CK_SECURITY_DOMAIN_THIRD_PARTY			| 0x00000003	| Third party protection domain
*/
#[allow(non_camel_case_types)]
pub type CK_JAVA_MIDP_SECURITY_DOMAIN = CK_ULONG;

/**
CK_MECHANISM_TYPE is a value that identifies a mechanism type.

Mechanism types are defined with the objects and mechanism descriptions that use them.

Vendor defined values for this type may also be specified. CKM_VENDOR_DEFINED

Mechanism types CKM_VENDOR_DEFINED and above are permanently reserved for token vendors. For interoperability, vendors should register their mechanism types through the PKCS process.

*/
#[allow(non_camel_case_types)]
pub type CK_MECHANISM_TYPE = CK_ULONG;

#[allow(non_camel_case_types)]
pub type CK_MECHANISM_TYPE_PTR = *const CK_MECHANISM_TYPE;

/**
CK_MECHANISM is a structure that specifies a particular mechanism and any parameters it requires

Note that pParameter is a “void” pointer, facilitating the passing of arbitrary values. Both the application and the Cryptoki library MUST ensure that the pointer can be safely cast to the expected type (i.e., without word-alignment errors)
*/
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_MECHANISM {
    /// the type of mechanism
    pub mechanism: CK_MECHANISM_TYPE,
    /// pointer to the parameter if required by the mechanism
    pub pParameter: CK_VOID,
    /// length in bytes of the parameter
    pub ulParameterLen: CK_ULONG,
}

#[allow(non_camel_case_types)]
pub type CK_MECHANISM_PTR = *const CK_MECHANISM;

/**
CK_MECHANISM_INFO is a structure that provides information about a particular mechanism

For some mechanisms, the ulMinKeySize and ulMaxKeySize fields have meaningless values.
*/
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_MECHANISM_INFO {
    /// the minimum size of the key for the mechanism (whether this is measured in bits or in bytes is mechanism-dependent)
    pub ulMinKeySize: CK_ULONG,
    /// the maximum size of the key for the mechanism (whether this is measured in bits or in bytes is mechanism-dependent)
    pub ulMaxKeySize: CK_ULONG,
    /** bit flags specifying mechanism capabilities
    The following table defines the flags field:

    Bit Flag				| Mask			| Meaning
    ------------------------|---------------|--------------------------------------------------------------------------
    CKF_HW					| 0x00000001	| True if the mechanism is performed by the device; false if the mechanism is performed in software
    CKF_MESSAGE_ENCRYPT		| 0x00000002	| True if the mechanism can be used with C_MessageEncryptInit
    CKF_MESSAGE_DECRYPT		| 0x00000004	| True if the mechanism can be used with C_MessageDecryptInit
    CKF_MESSAGE_SIGN		| 0x00000008	| True if the mechanism can be used with C_MessageSignInit
    CKF_MESSAGE_VERIFY		| 0x00000010	| True if the mechanism can be used with C_MessageVerifyInit
    CKF_MULTI_MESSAGE		| 0x00000020	| True if the mechanism can be used with C_*MessageBegin. One of CKF_MESSAGE_* flag must also be set.
    CKF_FIND_OBJECTS		| 0x00000040	| This flag can be passed in as a parameter to C_SessionCancel to cancel an active object search operation. Any other use of this flag is outside the scope of this standard.
    CKF_ENCRYPT				| 0x00000100	| True if the mechanism can be used with C_EncryptInit
    CKF_DECRYPT				| 0x00000200	| True if the mechanism can be used with C_DecryptInit
    CKF_DIGEST				| 0x00000400	| True if the mechanism can be used with C_DigestIni
    CKF_SIGN				| 0x00000800	| True if the mechanism can be used with C_SignInit
    CKF_SIGN_RECOVER		| 0x00001000	| True if the mechanism can be used with C_SignRecoverInit
    CKF_VERIFY				| 0x00002000	| True if the mechanism can be used with C_VerifyInit
    CKF_VERIFY_RECOVER		| 0x00004000	| True if the mechanism can be used with C_VerifyRecoverInit
    CKF_GENERATE			| 0x00008000	| True if the mechanism can be used with C_GenerateKey
    CKF_GENERATE_KEY_PAIR	| 0x00010000	| True if the mechanism can be used with C_GenerateKeyPair
    CKF_WRAP				| 0x00020000	| True if the mechanism can be used with C_WrapKey
    CKF_UNWRAP				| 0x00040000	| True if the mechanism can be used with C_UnwrapKey
    CKF_DERIVE				| 0x00080000	| True if the mechanism can be used with C_DeriveKey
    CKF_EXTENSION			| 0x80000000	| True if there is an extension to the flags; false if no extensions. MUST be false for this version
    */
    pub flags: CK_FLAGS,
}

#[allow(non_camel_case_types)]
pub type CK_MECHANISM_INFO_PTR = *const CK_MECHANISM_INFO;

/// CK_RV is a value that identifies the return value of a Cryptoki function
#[allow(non_camel_case_types)]
pub type CK_RV = CK_ULONG;

// FIXME: function types CK_C_XXX through CK_FUNCTION_LIST

#[allow(non_camel_case_types)]
pub type CK_NOTIFY =
    CK_CALLBACK_FUNCTION_3_POINTER<CK_RV, CK_SESSION_HANDLE, CK_NOTIFICATION, CK_VOID>;

/// CK_INTERFACE is a structure which contains an interface name with a function list and flag.
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_INTERFACE {
    /// the name of the interface
    pub pInterfaceName: CK_UTF8CHAR_PTR,
    /// the interface function list which must always begin with a CK_VERSION structure as the first field
    pub pFunctonList: CK_VOID, // FIXME: point to function list
    /** bit flags specifying interface capabilities

    The following table defines the flags field:

    Bit Flag				| Mask			| Meaning
    ------------------------|---------------|--------------------------------------------------------------------------
    CKF_INTERFACE_FORK_SAFE	| 0x00000001	| The returned interface will have fork tolerant semantics. When the application forks, each process will get its own copy of all session objects, session states, login states, and encryption states. Each process will also maintain access to token objects with their previously supplied handles.
    */
    pub flags: CK_FLAGS,
}

#[allow(non_camel_case_types)]
pub type CK_INTERFACE_PTR = *const CK_INTERFACE;

#[allow(non_camel_case_types)]
pub type CK_INTERFACE_PTR_PTR = *mut CK_INTERFACE_PTR;

#[allow(non_camel_case_types)]
pub type CK_CREATEMUTEX = CK_CALLBACK_FUNCTION_1_POINTER<CK_RV, CK_VOID>;

#[allow(non_camel_case_types)]
pub type CK_DESTROYMUTEX = CK_CALLBACK_FUNCTION_1_POINTER<CK_RV, CK_VOID>;

#[allow(non_camel_case_types)]
pub type CK_LOCKYMUTEX = CK_CALLBACK_FUNCTION_1_POINTER<CK_RV, CK_VOID>;

#[allow(non_camel_case_types)]
pub type CK_UNLOCKMUTEX = CK_CALLBACK_FUNCTION_1_POINTER<CK_RV, CK_VOID>;

/// CK_C_INITIALIZE_ARGS is a structure containing the optional arguments for the C_Initialize function. For this version of Cryptoki, these optional arguments are all concerned with the way the library deals with threads
#[repr(C)]
#[allow(non_snake_case)]
pub struct CK_C_INITIALIZE_ARGS {
    /// pointer to a function to use for creating mutex objects
    pub CreateMutex: CK_CREATEMUTEX,
    /// pointer to a function to use for destroying mutex objects
    pub DestroyMutex: CK_DESTROYMUTEX,
    /// pointer to a function to use for locking mutex objects
    pub LockMutex: CK_LOCKYMUTEX,
    /// pointer to a function to use for unlocking mutex objects
    pub UnlockMutex: CK_UNLOCKMUTEX,
    /** bit flags specifying options for C_Initialize

    The following table defines the flags field:

    Bit Flag							| Mask			| Meaning
    ------------------------------------|---------------|--------------------------------------------------------------------------
    CKF_LIBRARY_CANT_CREATE_OS_THREADS	| 0x00000001	| True if application threads which are executing calls to the library may not use native operating system calls to spawn new threads; false if they may
    CKF_OS_LOCKING_OK					| 0x00000002	| True if the library can use the native operation system threading model for locking; false otherwise
    */
    pub flags: CK_FLAGS,
    pub pReserved: CK_VOID,
}

#[allow(non_camel_case_types)]
pub type CK_C_INITIALIZE_ARGS_PTR = *const CK_C_INITIALIZE_ARGS;

pub enum Attribute {
    None,
    ByteArray(Vec<CK_BYTE>),
    BigInteger(Vec<CK_BYTE>),
    LocalString(Vec<CK_CHAR>),
    RFC2279String(Vec<CK_UTF8CHAR>),
}
