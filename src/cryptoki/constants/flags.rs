use super::super::data_types::*;
pub const CKF_TOKEN_PRESENT: CK_ULONG = 0x00000001;
pub const CKF_REMOVABLE_DEVICE: CK_ULONG = 0x00000002;
pub const CKF_HW_SLOT: CK_ULONG = 0x00000004;
