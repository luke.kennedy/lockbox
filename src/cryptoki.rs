pub mod constants;
pub mod data_types;
use constants::*;
use data_types::*;

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Initialize(_: CK_VOID) -> CK_RV {
    // println!("Hello!");
    // std::thread::sleep(std::time::Duration::new(100, 0));
    // let args: Option<&CK_C_INITIALIZE_ARGS>;
    // unsafe {
    //     args = pInitArgs.as_ref();
    // }
    // match args {
    //     Some(x) => println!("{}", x.flags),
    //     None => return CKR_ARGUMENTS_BAD,
    // }

    CKR_OK
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Finalize(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetInfo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetFunctionList(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetInterfaceList(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetInterface(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetSlotList(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetSlotInfo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetTokenInfo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_WaitForSlotEvent(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetMechanismList(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetMechanismInfo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_InitToken(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_InitPIN(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SetPIN(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_OpenSession(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_CloseSession(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_CloseAllSessions(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetSessionInfo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SessionCancel(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetOperationState(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SetOperationState(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Login(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_LoginUser(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Logout(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_CreateObject(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_CopyObject(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DestroyObject(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetObjectSize(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetAttributeValue(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SetAttributeValueo(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_FindObjectsInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_FindObjects(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_FindObjectsFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Encrypt(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageEncryptInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptMessage(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptMessageBegin(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_EncryptMessageNext(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageEncryptFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Decrypt(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageDecryptInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptMessage(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptMessageBegin(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptMessageNext(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageDecryptFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DigestInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Digest(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DigestUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DigestKey(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DigestFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Sign(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignRecoverInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignRecover(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageSignInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignMessage(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignMessageBegin(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignMessageNext(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageSignFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_Verify(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyRecoverInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyRecover(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageVerifyInit(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyMessage(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyMessageBegin(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_VerifyMessageNext(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_MessageVerifyFinal(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DigestEncryptUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptDigestUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SignEncryptUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DecryptVerifyUpdate(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GenerateKey(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GenerateKeyPair(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_WrapKey(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_UnwrapKey(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_DeriveKey(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_SeedRandom(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GenerateRandom(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_GetFunctionStatus(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}

#[no_mangle]
#[allow(non_snake_case)]
pub extern "C" fn C_CancelFunction(_: CK_VOID) -> CK_RV {
    CKR_FUNCTION_NOT_SUPPORTED
}
