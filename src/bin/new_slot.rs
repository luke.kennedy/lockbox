extern crate cryptoki;
extern crate diesel;

use self::cryptoki::*;
use self::diesel::prelude::*;
// use self::models::*;

fn main() {
    use cryptoki::schema::slots::dsl::*;

    let connection = establish_connection();
    let new_slot = vec![id.eq(uuid::Uuid::new_v4())];
    diesel::insert_into(slots)
        .values(new_slot)
        .execute(&connection)
        .expect("Failed to create new slot");
}
