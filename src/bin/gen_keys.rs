use cryptoki::security::{
    self,
    system::{ShareWithPassword, MAX_SHARES},
};
use dialoguer::*;
use std::{collections::HashMap, fs::*};

fn main() {
    let mut names_to_passwords: HashMap<String, String> = HashMap::new();
    let num_shares: u8 = Input::new()
        .with_prompt("Number of shares")
        .default(3)
        .interact_text()
        .expect("Failed to parse number of shares");
    if num_shares == 0 || num_shares > MAX_SHARES as u8 {
        panic!(
            "Must have at least one share and at most {} shares",
            MAX_SHARES
        );
    }
    let minimum_shares: u8 = Input::new()
        .with_prompt("Required shares for authentication")
        .default(1)
        .interact_text()
        .expect("Failed to parse minimum number of shares");
    if minimum_shares > num_shares {
        panic!("Minimum shares cannot be larger than total shares");
    }
    for _ in 0..num_shares {
        let share_name = Input::new()
            .with_prompt("Name of share (must be unique)")
            .interact_text()
            .expect("Failed to parse name of share");
        for (existing_name, _) in &names_to_passwords {
            if *existing_name == share_name {
                panic!("Duplicate share names! Must be unique")
            }
        }
        let pwd = Password::new()
            .with_prompt("Password")
            .interact()
            .expect("Failed to parse password");
        let confirm_pwd = Password::new()
            .with_prompt("Confirm Password")
            .interact()
            .expect("Failed to parse confirmed password");
        if pwd != confirm_pwd {
            panic!("Passwords are not the same");
        }
        if pwd.len() == 0 {
            panic!("Password cannot be empty");
        }
        names_to_passwords.insert(share_name, pwd);
    }
    println!("Generating shares...");
    let components =
        security::system::create_key_components(&names_to_passwords, minimum_shares, None).unwrap();
    println!("Verifying...");
    // Verify
    let mut names_to_shares: HashMap<String, ShareWithPassword> = HashMap::new();
    for share in &components {
        let name = &share.name;
        names_to_shares.insert(
            name.clone(),
            ShareWithPassword {
                password: names_to_passwords.get(name).unwrap(),
                share: share,
            },
        );
    }
    security::system::combine_key_components(&names_to_shares).unwrap();
    for part in &components {
        let data = serde_json::to_string(&part).unwrap();
        write(format!("{}.json", part.name), data.as_bytes()).expect("Failed to write file");
        println!("{}", data);
    }
}
