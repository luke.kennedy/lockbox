extern crate cryptoki;
extern crate diesel;

use self::cryptoki::*;
use self::diesel::prelude::*;
// use self::models::*;

fn main() {
    use cryptoki::schema::slots::dsl::*;

    let connection = establish_connection();

    let results = slots
        .limit(5)
        .select(id)
        .load::<uuid::Uuid>(&connection)
        .expect("Error loading IDs");

    println!("Displaying {} slots", results.len());
    for post in results {
        println!("{:?}", post);
        println!("----------\n");
    }
}
