use cryptoki::security;

fn main() {
    // TODO: use the resultant key to lock/unlock PG
    let system_key = security::system::cli_load_shares();
    println!("Success! {:?}", system_key);
}
