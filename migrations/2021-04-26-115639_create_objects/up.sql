-- Your SQL goes here

CREATE TABLE objects (
	id uuid PRIMARY KEY,
	slot_id uuid NOT NULL REFERENCES slots(id),
	name text
);