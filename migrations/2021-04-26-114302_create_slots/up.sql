-- Your SQL goes here

CREATE TABLE slots (
	id uuid PRIMARY KEY,
	name text,
	number integer
);