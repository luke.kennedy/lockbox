package main

import (
	"log"
	"syscall"

	"github.com/llkennedy/pkcs11"
)

const modulePath = `D:\Source\lockbox\target\debug\cryptoki.dll`

func main() {
	var err error

	dll := syscall.NewLazyDLL(modulePath)
	proc := dll.NewProc("C_Finalize")
	_ = proc
	err = dll.Load()
	if err != nil {
		log.Println("1")
		log.Fatalln(err)
	}
	ret, _, err := proc.Call(uintptr(0))
	log.Println(ret)
	log.Println(err)
	ctx := pkcs11.New(modulePath)
	log.Println("a")
	if ctx == nil {
		log.Println("2")
		log.Fatalln("could not create module")
	}
	log.Println("b")
	err = ctx.Initialize()
	log.Println("c")
	if err != nil {
		log.Println("3")
		log.Fatalln(err)
	}
	log.Println("d")
	log.Println("done")
}
